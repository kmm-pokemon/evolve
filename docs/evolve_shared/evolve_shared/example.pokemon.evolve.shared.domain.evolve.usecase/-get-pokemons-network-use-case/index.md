//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.usecase](../index.md)/[GetPokemonsNetworkUseCase](index.md)

# GetPokemonsNetworkUseCase

[common]\
public final class [GetPokemonsNetworkUseCase](index.md) implements KoinComponent

## Constructors

| | |
|---|---|
| [GetPokemonsNetworkUseCase](-get-pokemons-network-use-case.md) | [common]<br>public [GetPokemonsNetworkUseCase](index.md)[GetPokemonsNetworkUseCase](-get-pokemons-network-use-case.md)() |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;[invoke](invoke.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)offset, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)limit) |

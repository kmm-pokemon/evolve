//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared.domain.evolve.usecase](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [GetBerriesLocalUseCase](-get-berries-local-use-case/index.md) | [common]<br>public final class [GetBerriesLocalUseCase](-get-berries-local-use-case/index.md) implements KoinComponent |
| [GetBerriesNetworkUseCase](-get-berries-network-use-case/index.md) | [common]<br>public final class [GetBerriesNetworkUseCase](-get-berries-network-use-case/index.md) implements KoinComponent |
| [GetPokemonDetailLocalUseCase](-get-pokemon-detail-local-use-case/index.md) | [common]<br>public final class [GetPokemonDetailLocalUseCase](-get-pokemon-detail-local-use-case/index.md) implements KoinComponent |
| [GetPokemonDetailNetworkUseCase](-get-pokemon-detail-network-use-case/index.md) | [common]<br>public final class [GetPokemonDetailNetworkUseCase](-get-pokemon-detail-network-use-case/index.md) implements KoinComponent |
| [GetPokemonsLocalUseCase](-get-pokemons-local-use-case/index.md) | [common]<br>public final class [GetPokemonsLocalUseCase](-get-pokemons-local-use-case/index.md) implements KoinComponent |
| [GetPokemonsNetworkUseCase](-get-pokemons-network-use-case/index.md) | [common]<br>public final class [GetPokemonsNetworkUseCase](-get-pokemons-network-use-case/index.md) implements KoinComponent |
| [SetPokemonDetailLocalUseCase](-set-pokemon-detail-local-use-case/index.md) | [common]<br>public final class [SetPokemonDetailLocalUseCase](-set-pokemon-detail-local-use-case/index.md) implements KoinComponent |
| [SetPokemonsLocalUseCase](-set-pokemons-local-use-case/index.md) | [common]<br>public final class [SetPokemonsLocalUseCase](-set-pokemons-local-use-case/index.md) implements KoinComponent |

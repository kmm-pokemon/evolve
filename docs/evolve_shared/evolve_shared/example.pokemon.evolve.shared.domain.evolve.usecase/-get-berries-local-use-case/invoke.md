//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.usecase](../index.md)/[GetBerriesLocalUseCase](index.md)/[invoke](invoke.md)

# invoke

[common]\

public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[Berry](../../example.pokemon.evolve.shared.domain.evolve.entity/-berry/index.md)&gt;[invoke](invoke.md)()

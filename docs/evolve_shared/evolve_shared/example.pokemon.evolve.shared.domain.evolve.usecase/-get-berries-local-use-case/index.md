//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.usecase](../index.md)/[GetBerriesLocalUseCase](index.md)

# GetBerriesLocalUseCase

[common]\
public final class [GetBerriesLocalUseCase](index.md) implements KoinComponent

## Constructors

| | |
|---|---|
| [GetBerriesLocalUseCase](-get-berries-local-use-case.md) | [common]<br>public [GetBerriesLocalUseCase](index.md)[GetBerriesLocalUseCase](-get-berries-local-use-case.md)() |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[Berry](../../example.pokemon.evolve.shared.domain.evolve.entity/-berry/index.md)&gt;[invoke](invoke.md)() |

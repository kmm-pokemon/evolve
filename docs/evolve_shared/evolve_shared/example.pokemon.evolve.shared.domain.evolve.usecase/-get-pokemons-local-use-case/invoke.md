//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.usecase](../index.md)/[GetPokemonsLocalUseCase](index.md)/[invoke](invoke.md)

# invoke

[common]\

public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;[invoke](invoke.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)offset, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)limit, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)search, [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)isChoosen)

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.usecase](../index.md)/[GetPokemonsLocalUseCase](index.md)

# GetPokemonsLocalUseCase

[common]\
public final class [GetPokemonsLocalUseCase](index.md) implements KoinComponent

## Constructors

| | |
|---|---|
| [GetPokemonsLocalUseCase](-get-pokemons-local-use-case.md) | [common]<br>public [GetPokemonsLocalUseCase](index.md)[GetPokemonsLocalUseCase](-get-pokemons-local-use-case.md)() |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;[invoke](invoke.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)offset, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)limit, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)search, [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)isChoosen) |

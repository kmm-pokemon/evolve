//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonUrl](index.md)

# PokemonUrl

[common]\
@Serializable()

public class [PokemonUrl](index.md) implements RealmObject

## Constructors

| | |
|---|---|
| [PokemonUrl](-pokemon-url.md) | [common]<br>public [PokemonUrl](index.md)[PokemonUrl](-pokemon-url.md)() |

## Functions

| Name | Summary |
|---|---|
| [clone](clone.md) | [common]<br>public final [PokemonUrl](index.md)[clone](clone.md)() |
| [getName](get-name.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getName](get-name.md)() |
| [getOrder](get-order.md) | [common]<br>public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getOrder](get-order.md)() |
| [getPokemon](get-pokemon.md) | [common]<br>public final [Pokemon](../-pokemon/index.md)[getPokemon](get-pokemon.md)() |
| [getUrl](get-url.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getUrl](get-url.md)() |
| [isChosen](is-chosen.md) | [common]<br>public final [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[isChosen](is-chosen.md)() |
| [setChosen](set-chosen.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setChosen](set-chosen.md)([Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)isChosen) |
| [setName](set-name.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setName](set-name.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name) |
| [setOrder](set-order.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setOrder](set-order.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)order) |
| [setPokemon](set-pokemon.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setPokemon](set-pokemon.md)([Pokemon](../-pokemon/index.md)pokemon) |
| [setUrl](set-url.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setUrl](set-url.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)url) |

## Properties

| Name | Summary |
|---|---|
| [isChosen](is-chosen.md) | [common]<br>private [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[isChosen](is-chosen.md) |
| [name](index.md#964488717%2FProperties%2F442279350) | [common]<br>private [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[name](index.md#964488717%2FProperties%2F442279350) |
| [order](index.md#-1723581164%2FProperties%2F442279350) | [common]<br>private [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[order](index.md#-1723581164%2FProperties%2F442279350) |
| [pokemon](index.md#-1339772433%2FProperties%2F442279350) | [common]<br>private [Pokemon](../-pokemon/index.md)[pokemon](index.md#-1339772433%2FProperties%2F442279350) |
| [url](index.md#1365863251%2FProperties%2F442279350) | [common]<br>private [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[url](index.md#1365863251%2FProperties%2F442279350) |

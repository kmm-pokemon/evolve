//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonUrl](index.md)/[getPokemon](get-pokemon.md)

# getPokemon

[common]\

public final [Pokemon](../-pokemon/index.md)[getPokemon](get-pokemon.md)()

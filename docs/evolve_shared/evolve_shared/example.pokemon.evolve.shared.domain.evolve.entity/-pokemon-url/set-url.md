//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonUrl](index.md)/[setUrl](set-url.md)

# setUrl

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setUrl](set-url.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)url)

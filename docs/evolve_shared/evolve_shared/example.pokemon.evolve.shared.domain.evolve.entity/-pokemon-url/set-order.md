//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonUrl](index.md)/[setOrder](set-order.md)

# setOrder

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setOrder](set-order.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)order)

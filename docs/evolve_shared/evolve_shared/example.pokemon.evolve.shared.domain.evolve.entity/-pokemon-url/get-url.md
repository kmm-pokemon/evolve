//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonUrl](index.md)/[getUrl](get-url.md)

# getUrl

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getUrl](get-url.md)()

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonUrl](index.md)/[isChosen](is-chosen.md)

# isChosen

[common]\

public final [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[isChosen](is-chosen.md)()

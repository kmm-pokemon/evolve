//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonType](index.md)/[setType](set-type.md)

# setType

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setType](set-type.md)([PokemonUrl](../-pokemon-url/index.md)type)

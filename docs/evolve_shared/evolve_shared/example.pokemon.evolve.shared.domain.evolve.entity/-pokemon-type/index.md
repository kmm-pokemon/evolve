//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonType](index.md)

# PokemonType

[common]\
@Serializable()

public class [PokemonType](index.md) implements RealmObject

## Constructors

| | |
|---|---|
| [PokemonType](-pokemon-type.md) | [common]<br>public [PokemonType](index.md)[PokemonType](-pokemon-type.md)() |

## Functions

| Name | Summary |
|---|---|
| [clone](clone.md) | [common]<br>public final [PokemonType](index.md)[clone](clone.md)() |
| [getType](get-type.md) | [common]<br>public final [PokemonUrl](../-pokemon-url/index.md)[getType](get-type.md)() |
| [setType](set-type.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setType](set-type.md)([PokemonUrl](../-pokemon-url/index.md)type) |

## Properties

| Name | Summary |
|---|---|
| [type](index.md#-1645029901%2FProperties%2F442279350) | [common]<br>private [PokemonUrl](../-pokemon-url/index.md)[type](index.md#-1645029901%2FProperties%2F442279350) |

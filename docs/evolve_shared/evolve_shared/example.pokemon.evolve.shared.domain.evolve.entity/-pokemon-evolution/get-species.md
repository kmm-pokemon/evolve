//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonEvolution](index.md)/[getSpecies](get-species.md)

# getSpecies

[common]\

public final [PokemonUrl](../-pokemon-url/index.md)[getSpecies](get-species.md)()

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonStat](index.md)

# PokemonStat

[common]\
@Serializable()

public class [PokemonStat](index.md) implements RealmObject

## Constructors

| | |
|---|---|
| [PokemonStat](-pokemon-stat.md) | [common]<br>public [PokemonStat](index.md)[PokemonStat](-pokemon-stat.md)() |

## Functions

| Name | Summary |
|---|---|
| [clone](clone.md) | [common]<br>public final [PokemonStat](index.md)[clone](clone.md)() |
| [getBase_stat](get-base_stat.md) | [common]<br>public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getBase_stat](get-base_stat.md)() |
| [getStat](get-stat.md) | [common]<br>public final [PokemonUrl](../-pokemon-url/index.md)[getStat](get-stat.md)() |
| [setBase_stat](set-base_stat.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setBase_stat](set-base_stat.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)base_stat) |
| [setStat](set-stat.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setStat](set-stat.md)([PokemonUrl](../-pokemon-url/index.md)stat) |

## Properties

| Name | Summary |
|---|---|
| [base_stat](index.md#575490929%2FProperties%2F442279350) | [common]<br>private [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[base_stat](index.md#575490929%2FProperties%2F442279350) |
| [stat](index.md#218316211%2FProperties%2F442279350) | [common]<br>private [PokemonUrl](../-pokemon-url/index.md)[stat](index.md#218316211%2FProperties%2F442279350) |

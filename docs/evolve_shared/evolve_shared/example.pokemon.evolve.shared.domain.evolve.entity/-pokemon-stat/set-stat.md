//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonStat](index.md)/[setStat](set-stat.md)

# setStat

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setStat](set-stat.md)([PokemonUrl](../-pokemon-url/index.md)stat)

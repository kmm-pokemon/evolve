//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonStat](index.md)/[setBase_stat](set-base_stat.md)

# setBase_stat

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setBase_stat](set-base_stat.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)base_stat)

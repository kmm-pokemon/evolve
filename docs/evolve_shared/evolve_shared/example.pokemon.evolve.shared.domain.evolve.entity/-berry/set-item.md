//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Berry](index.md)/[setItem](set-item.md)

# setItem

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setItem](set-item.md)([PokemonUrl](../-pokemon-url/index.md)item)

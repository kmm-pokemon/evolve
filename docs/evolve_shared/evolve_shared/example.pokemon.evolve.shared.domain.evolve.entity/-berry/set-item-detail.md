//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Berry](index.md)/[setItemDetail](set-item-detail.md)

# setItemDetail

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setItemDetail](set-item-detail.md)([PokemonItemResp](../../example.pokemon.evolve.shared.data.evolve.network.response/-pokemon-item-resp/index.md)itemDetail)

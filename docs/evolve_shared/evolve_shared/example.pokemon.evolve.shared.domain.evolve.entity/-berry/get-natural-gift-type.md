//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Berry](index.md)/[getNaturalGiftType](get-natural-gift-type.md)

# getNaturalGiftType

[common]\

public final [PokemonUrl](../-pokemon-url/index.md)[getNaturalGiftType](get-natural-gift-type.md)()

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)/[setTypes](set-types.md)

# setTypes

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setTypes](set-types.md)(@Serializable(with = [RealmListPokemonTypeSerializer.class](../../example.pokemon.evolve.shared.external.utility/-realm-list-pokemon-type-serializer/index.md))RealmList&lt;[PokemonType](../-pokemon-type/index.md)&gt;types)

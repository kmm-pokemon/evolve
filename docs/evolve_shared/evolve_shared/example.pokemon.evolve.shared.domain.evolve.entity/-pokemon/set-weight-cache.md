//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)/[setWeightCache](set-weight-cache.md)

# setWeightCache

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setWeightCache](set-weight-cache.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)weightCache)

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)

# Pokemon

[common]\
@Serializable()

public class [Pokemon](index.md) implements RealmObject

## Constructors

| | |
|---|---|
| [Pokemon](-pokemon.md) | [common]<br>public [Pokemon](index.md)[Pokemon](-pokemon.md)() |

## Functions

| Name | Summary |
|---|---|
| [clone](clone.md) | [common]<br>public final [Pokemon](index.md)[clone](clone.md)() |
| [getEvolution](get-evolution.md) | [common]<br>public final [PokemonEvolution](../-pokemon-evolution/index.md)[getEvolution](get-evolution.md)() |
| [getId](get-id.md) | [common]<br>public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getId](get-id.md)() |
| [getImage](get-image.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getImage](get-image.md)() |
| [getName](get-name.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getName](get-name.md)() |
| [getSpecies](get-species.md) | [common]<br>public final [PokemonUrl](../-pokemon-url/index.md)[getSpecies](get-species.md)() |
| [getSprites](get-sprites.md) | [common]<br>public final [PokemonSpritesResp](../../example.pokemon.evolve.shared.data.evolve.network.response/-pokemon-sprites-resp/index.md)[getSprites](get-sprites.md)() |
| [getStats](get-stats.md) | [common]<br>public final RealmList&lt;[PokemonStat](../-pokemon-stat/index.md)&gt;[getStats](get-stats.md)() |
| [getTypes](get-types.md) | [common]<br>public final RealmList&lt;[PokemonType](../-pokemon-type/index.md)&gt;[getTypes](get-types.md)() |
| [getWeight](get-weight.md) | [common]<br>public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getWeight](get-weight.md)() |
| [getWeightCache](get-weight-cache.md) | [common]<br>public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getWeightCache](get-weight-cache.md)() |
| [setEvolution](set-evolution.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setEvolution](set-evolution.md)([PokemonEvolution](../-pokemon-evolution/index.md)evolution) |
| [setId](set-id.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setId](set-id.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)id) |
| [setImage](set-image.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setImage](set-image.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)image) |
| [setName](set-name.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setName](set-name.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name) |
| [setSpecies](set-species.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSpecies](set-species.md)([PokemonUrl](../-pokemon-url/index.md)species) |
| [setSprites](set-sprites.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSprites](set-sprites.md)([PokemonSpritesResp](../../example.pokemon.evolve.shared.data.evolve.network.response/-pokemon-sprites-resp/index.md)sprites) |
| [setStats](set-stats.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setStats](set-stats.md)(@Serializable(with = [RealmListPokemonStatSerializer.class](../../example.pokemon.evolve.shared.external.utility/-realm-list-pokemon-stat-serializer/index.md))RealmList&lt;[PokemonStat](../-pokemon-stat/index.md)&gt;stats) |
| [setTypes](set-types.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setTypes](set-types.md)(@Serializable(with = [RealmListPokemonTypeSerializer.class](../../example.pokemon.evolve.shared.external.utility/-realm-list-pokemon-type-serializer/index.md))RealmList&lt;[PokemonType](../-pokemon-type/index.md)&gt;types) |
| [setWeight](set-weight.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setWeight](set-weight.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)weight) |
| [setWeightCache](set-weight-cache.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setWeightCache](set-weight-cache.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)weightCache) |

## Properties

| Name | Summary |
|---|---|
| [evolution](index.md#-357651060%2FProperties%2F442279350) | [common]<br>private [PokemonEvolution](../-pokemon-evolution/index.md)[evolution](index.md#-357651060%2FProperties%2F442279350) |
| [id](index.md#-675809960%2FProperties%2F442279350) | [common]<br>private [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[id](index.md#-675809960%2FProperties%2F442279350) |
| [image](index.md#1114539180%2FProperties%2F442279350) | [common]<br>private [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[image](index.md#1114539180%2FProperties%2F442279350) |
| [name](index.md#-12040088%2FProperties%2F442279350) | [common]<br>private [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[name](index.md#-12040088%2FProperties%2F442279350) |
| [species](index.md#-1691743893%2FProperties%2F442279350) | [common]<br>private [PokemonUrl](../-pokemon-url/index.md)[species](index.md#-1691743893%2FProperties%2F442279350) |
| [sprites](index.md#-188126055%2FProperties%2F442279350) | [common]<br>private [PokemonSpritesResp](../../example.pokemon.evolve.shared.data.evolve.network.response/-pokemon-sprites-resp/index.md)[sprites](index.md#-188126055%2FProperties%2F442279350) |
| [stats](index.md#1834802664%2FProperties%2F442279350) | [common]<br>@Serializable(with = [RealmListPokemonStatSerializer.class](../../example.pokemon.evolve.shared.external.utility/-realm-list-pokemon-stat-serializer/index.md))<br>private RealmList&lt;[PokemonStat](../-pokemon-stat/index.md)&gt;[stats](index.md#1834802664%2FProperties%2F442279350) |
| [types](index.md#-48729266%2FProperties%2F442279350) | [common]<br>@Serializable(with = [RealmListPokemonTypeSerializer.class](../../example.pokemon.evolve.shared.external.utility/-realm-list-pokemon-type-serializer/index.md))<br>private RealmList&lt;[PokemonType](../-pokemon-type/index.md)&gt;[types](index.md#-48729266%2FProperties%2F442279350) |
| [weight](index.md#-2141292645%2FProperties%2F442279350) | [common]<br>private [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[weight](index.md#-2141292645%2FProperties%2F442279350) |
| [weightCache](index.md#-378962563%2FProperties%2F442279350) | [common]<br>private [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[weightCache](index.md#-378962563%2FProperties%2F442279350) |

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)/[getName](get-name.md)

# getName

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getName](get-name.md)()

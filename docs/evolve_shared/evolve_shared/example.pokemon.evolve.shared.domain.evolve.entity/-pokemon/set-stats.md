//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)/[setStats](set-stats.md)

# setStats

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setStats](set-stats.md)(@Serializable(with = [RealmListPokemonStatSerializer.class](../../example.pokemon.evolve.shared.external.utility/-realm-list-pokemon-stat-serializer/index.md))RealmList&lt;[PokemonStat](../-pokemon-stat/index.md)&gt;stats)

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.constant](../index.md)/[EvolveConstant](index.md)

# EvolveConstant

[android]\
public class [EvolveConstant](index.md)

## Functions

| Name | Summary |
|---|---|
| [getTYPE_KEY](get-t-y-p-e_-k-e-y.md) | [android]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getTYPE_KEY](get-t-y-p-e_-k-e-y.md)() |

## Properties

| Name | Summary |
|---|---|
| [INSTANCE](index.md#956069881%2FProperties%2F-1637274268) | [android]<br>public final static [EvolveConstant](index.md)[INSTANCE](index.md#956069881%2FProperties%2F-1637274268) |
| [TYPE_KEY](index.md#-821939084%2FProperties%2F-1637274268) | [android]<br>private final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[TYPE_KEY](index.md#-821939084%2FProperties%2F-1637274268) |

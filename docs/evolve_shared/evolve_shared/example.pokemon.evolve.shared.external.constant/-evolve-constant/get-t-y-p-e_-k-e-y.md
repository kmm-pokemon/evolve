//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.constant](../index.md)/[EvolveConstant](index.md)/[getTYPE_KEY](get-t-y-p-e_-k-e-y.md)

# getTYPE_KEY

[android]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getTYPE_KEY](get-t-y-p-e_-k-e-y.md)()

//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared.external.constant](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [EvolveConstant](-evolve-constant/index.md) | [android]<br>public class [EvolveConstant](-evolve-constant/index.md) |

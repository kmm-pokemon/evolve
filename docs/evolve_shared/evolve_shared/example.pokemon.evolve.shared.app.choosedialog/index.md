//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared.app.choosedialog](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [ChooseDialogFragment](-choose-dialog-fragment/index.md) | [android]<br>public final class [ChooseDialogFragment](-choose-dialog-fragment/index.md) extends BaseDialogFragment&lt;&lt;Error class: unknown class&gt;&gt; |

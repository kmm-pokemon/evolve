//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared.data.evolve](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [EvolveRepositoryImpl](-evolve-repository-impl/index.md) | [common]<br>public final class [EvolveRepositoryImpl](-evolve-repository-impl/index.md) implements KoinComponent, [EvolveRepository](../example.pokemon.evolve.shared.domain.evolve/-evolve-repository/index.md) |

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve](../index.md)/[EvolveRepositoryImpl](index.md)/[getPokemonDetailNetwork](get-pokemon-detail-network.md)

# getPokemonDetailNetwork

[common]\

public [Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)[getPokemonDetailNetwork](get-pokemon-detail-network.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name)

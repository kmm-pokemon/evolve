//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve](../index.md)/[EvolveRepositoryImpl](index.md)

# EvolveRepositoryImpl

[common]\
public final class [EvolveRepositoryImpl](index.md) implements KoinComponent, [EvolveRepository](../../example.pokemon.evolve.shared.domain.evolve/-evolve-repository/index.md)

## Constructors

| | |
|---|---|
| [EvolveRepositoryImpl](-evolve-repository-impl.md) | [common]<br>public [EvolveRepositoryImpl](index.md)[EvolveRepositoryImpl](-evolve-repository-impl.md)() |

## Functions

| Name | Summary |
|---|---|
| [getBerriesLocal](get-berries-local.md) | [common]<br>public [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[Berry](../../example.pokemon.evolve.shared.domain.evolve.entity/-berry/index.md)&gt;[getBerriesLocal](get-berries-local.md)() |
| [getBerriesNetwork](get-berries-network.md) | [common]<br>public [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[Berry](../../example.pokemon.evolve.shared.domain.evolve.entity/-berry/index.md)&gt;[getBerriesNetwork](get-berries-network.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)offset, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)limit) |
| [getPokemonDetailLocal](get-pokemon-detail-local.md) | [common]<br>public [Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)[getPokemonDetailLocal](get-pokemon-detail-local.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name) |
| [getPokemonDetailNetwork](get-pokemon-detail-network.md) | [common]<br>public [Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)[getPokemonDetailNetwork](get-pokemon-detail-network.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name) |
| [getPokemonsLocal](get-pokemons-local.md) | [common]<br>public [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;[getPokemonsLocal](get-pokemons-local.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)offset, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)limit, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)search, [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)isChosen) |
| [getPokemonsNetwork](get-pokemons-network.md) | [common]<br>public [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;[getPokemonsNetwork](get-pokemons-network.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)offset, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)limit) |
| [setPokemonDetailLocal](set-pokemon-detail-local.md) | [common]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setPokemonDetailLocal](set-pokemon-detail-local.md)([Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)pokemon) |
| [setPokemonsLocal](set-pokemons-local.md) | [common]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setPokemonsLocal](set-pokemons-local.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;pokemons) |

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.onboarding](../index.md)/[OnBoardingViewModel](index.md)/[next](next.md)

# next

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[next](next.md)()

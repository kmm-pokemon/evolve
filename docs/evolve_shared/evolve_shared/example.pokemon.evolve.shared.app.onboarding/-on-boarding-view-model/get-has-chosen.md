//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.onboarding](../index.md)/[OnBoardingViewModel](index.md)/[getHasChosen](get-has-chosen.md)

# getHasChosen

[common]\

public final MutableStateFlow&lt;[Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)&gt;[getHasChosen](get-has-chosen.md)()

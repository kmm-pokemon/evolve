//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.onboarding](../index.md)/[OnBoardingViewModel](index.md)

# OnBoardingViewModel

[common]\
public final class [OnBoardingViewModel](index.md) extends BaseViewModel

## Constructors

| | |
|---|---|
| [OnBoardingViewModel](-on-boarding-view-model.md) | [common]<br>public [OnBoardingViewModel](index.md)[OnBoardingViewModel](-on-boarding-view-model.md)() |

## Functions

| Name | Summary |
|---|---|
| [choose](choose.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[choose](choose.md)() |
| [getHasChosen](get-has-chosen.md) | [common]<br>public final MutableStateFlow&lt;[Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)&gt;[getHasChosen](get-has-chosen.md)() |
| [getOnNext](get-on-next.md) | [common]<br>public final MutableStateFlow&lt;[Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)&gt;[getOnNext](get-on-next.md)() |
| [next](next.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[next](next.md)() |

## Properties

| Name | Summary |
|---|---|
| [hasChosen](index.md#1704964807%2FProperties%2F442279350) | [common]<br>private final MutableStateFlow&lt;[Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)&gt;[hasChosen](index.md#1704964807%2FProperties%2F442279350) |
| [onNext](index.md#-1239940587%2FProperties%2F442279350) | [common]<br>private final MutableStateFlow&lt;[Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)&gt;[onNext](index.md#-1239940587%2FProperties%2F442279350) |

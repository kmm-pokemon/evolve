//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared.app.onboarding](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [OnBoardingAdapter](-on-boarding-adapter/index.md) | [android]<br>public final class [OnBoardingAdapter](-on-boarding-adapter/index.md) extends [RecyclerView.Adapter](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.Adapter.html)&lt;[OnBoardingAdapter.ViewHolder](-on-boarding-adapter/-view-holder/index.md)&gt; |
| [OnBoardingFragment](-on-boarding-fragment/index.md) | [android]<br>public final class [OnBoardingFragment](-on-boarding-fragment/index.md) extends BaseFragment&lt;&lt;Error class: unknown class&gt;&gt; |
| [OnBoardingViewModel](-on-boarding-view-model/index.md) | [common]<br>public final class [OnBoardingViewModel](-on-boarding-view-model/index.md) extends BaseViewModel |

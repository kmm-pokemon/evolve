//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.onboarding](../index.md)/[OnBoardingAdapter](index.md)/[OnBoardingAdapter](-on-boarding-adapter.md)

# OnBoardingAdapter

[android]\

public [OnBoardingAdapter](index.md)[OnBoardingAdapter](-on-boarding-adapter.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)&gt;items)

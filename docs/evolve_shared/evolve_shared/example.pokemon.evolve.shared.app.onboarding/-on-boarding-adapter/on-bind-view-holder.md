//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.onboarding](../index.md)/[OnBoardingAdapter](index.md)/[onBindViewHolder](on-bind-view-holder.md)

# onBindViewHolder

[android]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onBindViewHolder](on-bind-view-holder.md)([OnBoardingAdapter.ViewHolder](-view-holder/index.md)viewHolder, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)position)

//[evolve_shared](../../../../index.md)/[example.pokemon.evolve.shared.app.pokemondetail](../../index.md)/[BerryAdapter](../index.md)/[ViewHolder](index.md)/[bind](bind.md)

# bind

[android]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[bind](bind.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)position)

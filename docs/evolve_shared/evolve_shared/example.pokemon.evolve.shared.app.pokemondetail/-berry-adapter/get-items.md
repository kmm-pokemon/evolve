//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemondetail](../index.md)/[BerryAdapter](index.md)/[getItems](get-items.md)

# getItems

[android]\

public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;Berry&gt;[getItems](get-items.md)()

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonDetailViewModel](index.md)/[getOnEvolve](get-on-evolve.md)

# getOnEvolve

[common]\

public final MutableStateFlow&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;[getOnEvolve](get-on-evolve.md)()

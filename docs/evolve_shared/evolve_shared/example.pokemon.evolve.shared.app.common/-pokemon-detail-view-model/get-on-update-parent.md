//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonDetailViewModel](index.md)/[getOnUpdateParent](get-on-update-parent.md)

# getOnUpdateParent

[common]\

public final MutableStateFlow&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;[getOnUpdateParent](get-on-update-parent.md)()

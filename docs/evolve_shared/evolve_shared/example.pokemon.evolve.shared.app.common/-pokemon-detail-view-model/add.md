//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonDetailViewModel](index.md)/[add](add.md)

# add

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[add](add.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)nameArg)

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonDetailViewModel](index.md)/[getNextPokemon](get-next-pokemon.md)

# getNextPokemon

[common]\

public final MutableStateFlow&lt;[Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)&gt;[getNextPokemon](get-next-pokemon.md)()

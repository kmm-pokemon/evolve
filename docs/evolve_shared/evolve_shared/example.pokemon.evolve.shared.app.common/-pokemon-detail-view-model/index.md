//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonDetailViewModel](index.md)

# PokemonDetailViewModel

[common]\
public final class [PokemonDetailViewModel](index.md) extends BaseViewModel implements KoinComponent

## Constructors

| | |
|---|---|
| [PokemonDetailViewModel](-pokemon-detail-view-model.md) | [common]<br>public [PokemonDetailViewModel](index.md)[PokemonDetailViewModel](-pokemon-detail-view-model.md)() |

## Functions

| Name | Summary |
|---|---|
| [add](add.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[add](add.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)nameArg) |
| [checkEvolution](check-evolution.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[checkEvolution](check-evolution.md)([PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)pokemonEvolution, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)name) |
| [delete](delete.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[delete](delete.md)() |
| [evolve](evolve.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[evolve](evolve.md)() |
| [feed](feed.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[feed](feed.md)([Berry](../../example.pokemon.evolve.shared.domain.evolve.entity/-berry/index.md)berry) |
| [getBerries](get-berries.md) | [common]<br>public final MutableStateFlow&lt;[List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[Berry](../../example.pokemon.evolve.shared.domain.evolve.entity/-berry/index.md)&gt;&gt;[getBerries](get-berries.md)() |
| [getBerriesLocal](get-berries-local.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[getBerriesLocal](get-berries-local.md)() |
| [getId](get-id.md) | [common]<br>public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getId](get-id.md)() |
| [getName](get-name.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getName](get-name.md)() |
| [getNextPokemon](get-next-pokemon.md) | [common]<br>public final MutableStateFlow&lt;[Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)&gt;[getNextPokemon](get-next-pokemon.md)() |
| [getOnEvolve](get-on-evolve.md) | [common]<br>public final MutableStateFlow&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;[getOnEvolve](get-on-evolve.md)() |
| [getOnUpdateParent](get-on-update-parent.md) | [common]<br>public final MutableStateFlow&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;[getOnUpdateParent](get-on-update-parent.md)() |
| [getPokemon](get-pokemon.md) | [common]<br>public final MutableStateFlow&lt;[Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)&gt;[getPokemon](get-pokemon.md)() |
| [getPokemonDetailLocal](get-pokemon-detail-local.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[getPokemonDetailLocal](get-pokemon-detail-local.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)nameArg) |
| [setBerries](set-berries.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setBerries](set-berries.md)(MutableStateFlow&lt;[List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[Berry](../../example.pokemon.evolve.shared.domain.evolve.entity/-berry/index.md)&gt;&gt;berries) |
| [setId](set-id.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setId](set-id.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)id) |
| [setName](set-name.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setName](set-name.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name) |
| [setNextPokemon](set-next-pokemon.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setNextPokemon](set-next-pokemon.md)(MutableStateFlow&lt;[Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)&gt;nextPokemon) |
| [setOnEvolve](set-on-evolve.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setOnEvolve](set-on-evolve.md)(MutableStateFlow&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;onEvolve) |
| [setOnUpdateParent](set-on-update-parent.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setOnUpdateParent](set-on-update-parent.md)(MutableStateFlow&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;onUpdateParent) |
| [setPokemon](set-pokemon.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setPokemon](set-pokemon.md)(MutableStateFlow&lt;[Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)&gt;pokemon) |

## Properties

| Name | Summary |
|---|---|
| [berries](index.md#1178260280%2FProperties%2F442279350) | [common]<br>private MutableStateFlow&lt;[List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[Berry](../../example.pokemon.evolve.shared.domain.evolve.entity/-berry/index.md)&gt;&gt;[berries](index.md#1178260280%2FProperties%2F442279350) |
| [id](index.md#-1029249421%2FProperties%2F442279350) | [common]<br>private [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[id](index.md#-1029249421%2FProperties%2F442279350) |
| [name](index.md#-364945725%2FProperties%2F442279350) | [common]<br>private [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[name](index.md#-364945725%2FProperties%2F442279350) |
| [nextPokemon](index.md#-784412404%2FProperties%2F442279350) | [common]<br>private MutableStateFlow&lt;[Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)&gt;[nextPokemon](index.md#-784412404%2FProperties%2F442279350) |
| [onEvolve](index.md#-2121255502%2FProperties%2F442279350) | [common]<br>private MutableStateFlow&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;[onEvolve](index.md#-2121255502%2FProperties%2F442279350) |
| [onUpdateParent](index.md#44109724%2FProperties%2F442279350) | [common]<br>private MutableStateFlow&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;[onUpdateParent](index.md#44109724%2FProperties%2F442279350) |
| [pokemon](index.md#1667169657%2FProperties%2F442279350) | [common]<br>private MutableStateFlow&lt;[Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)&gt;[pokemon](index.md#1667169657%2FProperties%2F442279350) |

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonDetailViewModel](index.md)/[setOnUpdateParent](set-on-update-parent.md)

# setOnUpdateParent

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setOnUpdateParent](set-on-update-parent.md)(MutableStateFlow&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;onUpdateParent)

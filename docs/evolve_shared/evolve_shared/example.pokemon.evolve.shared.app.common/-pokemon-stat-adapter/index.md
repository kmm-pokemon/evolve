//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonStatAdapter](index.md)

# PokemonStatAdapter

[android]\
public final class [PokemonStatAdapter](index.md) extends [RecyclerView.Adapter](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.Adapter.html)&lt;[PokemonStatAdapter.ViewHolder](-view-holder/index.md)&gt;

## Constructors

| | |
|---|---|
| [PokemonStatAdapter](-pokemon-stat-adapter.md) | [android]<br>public [PokemonStatAdapter](index.md)[PokemonStatAdapter](-pokemon-stat-adapter.md)([Double](https://developer.android.com/reference/kotlin/java/lang/Double.html)widthRatio, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)height) |

## Types

| Name | Summary |
|---|---|
| [ViewHolder](-view-holder/index.md) | [android]<br>public final class [ViewHolder](-view-holder/index.md) extends [RecyclerView.ViewHolder](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.ViewHolder.html) |

## Functions

| Name | Summary |
|---|---|
| [addItem](add-item.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[addItem](add-item.md)(Function0&lt;[Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;process) |
| [clear](clear.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[clear](clear.md)() |
| [getItemCount](get-item-count.md) | [android]<br>public [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getItemCount](get-item-count.md)() |
| [getItems](get-items.md) | [android]<br>public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;PokemonStat&gt;[getItems](get-items.md)() |
| [onBindViewHolder](on-bind-view-holder.md) | [android]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onBindViewHolder](on-bind-view-holder.md)([PokemonStatAdapter.ViewHolder](-view-holder/index.md)viewHolder, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)position) |
| [onCreateViewHolder](on-create-view-holder.md) | [android]<br>public [PokemonStatAdapter.ViewHolder](-view-holder/index.md)[onCreateViewHolder](on-create-view-holder.md)([ViewGroup](https://developer.android.com/reference/kotlin/android/view/ViewGroup.html)viewGroup, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)viewType) |
| [setItems](set-items.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setItems](set-items.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;PokemonStat&gt;items) |

## Properties

| Name | Summary |
|---|---|
| [items](index.md#1762793284%2FProperties%2F-1637274268) | [android]<br>private [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;PokemonStat&gt;[items](index.md#1762793284%2FProperties%2F-1637274268) |

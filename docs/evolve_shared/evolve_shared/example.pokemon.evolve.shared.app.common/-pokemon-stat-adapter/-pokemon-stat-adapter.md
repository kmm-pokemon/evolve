//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonStatAdapter](index.md)/[PokemonStatAdapter](-pokemon-stat-adapter.md)

# PokemonStatAdapter

[android]\

public [PokemonStatAdapter](index.md)[PokemonStatAdapter](-pokemon-stat-adapter.md)([Double](https://developer.android.com/reference/kotlin/java/lang/Double.html)widthRatio, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)height)

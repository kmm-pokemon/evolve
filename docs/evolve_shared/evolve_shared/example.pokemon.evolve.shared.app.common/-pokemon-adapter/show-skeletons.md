//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonAdapter](index.md)/[showSkeletons](show-skeletons.md)

# showSkeletons

[android]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[showSkeletons](show-skeletons.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)size)

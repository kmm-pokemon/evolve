//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonAdapter](index.md)/[onCreateViewHolder](on-create-view-holder.md)

# onCreateViewHolder

[android]\

public [PokemonAdapter.ViewHolder](-view-holder/index.md)[onCreateViewHolder](on-create-view-holder.md)([ViewGroup](https://developer.android.com/reference/kotlin/android/view/ViewGroup.html)viewGroup, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)viewType)

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonAdapter](index.md)/[getOnSelected](get-on-selected.md)

# getOnSelected

[android]\

public final Function2&lt;[View](https://developer.android.com/reference/kotlin/android/view/View.html), PokemonUrl, [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;[getOnSelected](get-on-selected.md)()

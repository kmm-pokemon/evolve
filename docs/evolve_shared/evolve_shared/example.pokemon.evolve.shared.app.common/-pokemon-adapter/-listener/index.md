//[evolve_shared](../../../../index.md)/[example.pokemon.evolve.shared.app.common](../../index.md)/[PokemonAdapter](../index.md)/[Listener](index.md)

# Listener

[android]\
public final class [Listener](index.md) extends [RecyclerView.OnScrollListener](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.OnScrollListener.html)

## Constructors

| | |
|---|---|
| [PokemonAdapter.Listener](-pokemon-adapter.-listener.md) | [android]<br>public [PokemonAdapter.Listener](index.md)[PokemonAdapter.Listener](-pokemon-adapter.-listener.md)([SwipeRefreshLayout](https://developer.android.com/reference/kotlin/androidx/swiperefreshlayout/widget/SwipeRefreshLayout.html)swipeRefreshLayout) |

## Functions

| Name | Summary |
|---|---|
| [onScrolled](on-scrolled.md) | [android]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onScrolled](on-scrolled.md)([RecyclerView](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.html)recyclerView, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)dx, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)dy) |

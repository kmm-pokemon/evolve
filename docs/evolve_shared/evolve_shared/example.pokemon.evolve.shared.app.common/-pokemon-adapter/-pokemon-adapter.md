//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonAdapter](index.md)/[PokemonAdapter](-pokemon-adapter.md)

# PokemonAdapter

[android]\

public [PokemonAdapter](index.md)[PokemonAdapter](-pokemon-adapter.md)([Double](https://developer.android.com/reference/kotlin/java/lang/Double.html)widthRatio, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)height, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)radius, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)elevation)

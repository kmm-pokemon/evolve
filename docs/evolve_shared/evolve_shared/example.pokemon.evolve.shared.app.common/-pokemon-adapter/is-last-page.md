//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonAdapter](index.md)/[isLastPage](is-last-page.md)

# isLastPage

[android]\

public final [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[isLastPage](is-last-page.md)()

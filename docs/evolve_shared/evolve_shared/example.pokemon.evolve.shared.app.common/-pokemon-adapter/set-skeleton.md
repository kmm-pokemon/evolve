//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonAdapter](index.md)/[setSkeleton](set-skeleton.md)

# setSkeleton

[android]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSkeleton](set-skeleton.md)(PokemonUrlskeleton)

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve](../index.md)/[EvolveRepository](index.md)/[getBerriesLocal](get-berries-local.md)

# getBerriesLocal

[common]\

public abstract [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[Berry](../../example.pokemon.evolve.shared.domain.evolve.entity/-berry/index.md)&gt;[getBerriesLocal](get-berries-local.md)()

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve](../index.md)/[EvolveRepository](index.md)/[setPokemonDetailLocal](set-pokemon-detail-local.md)

# setPokemonDetailLocal

[common]\

public abstract [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setPokemonDetailLocal](set-pokemon-detail-local.md)([Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)pokemon)

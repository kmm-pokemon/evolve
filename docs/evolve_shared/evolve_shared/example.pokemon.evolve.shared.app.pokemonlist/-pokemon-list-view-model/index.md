//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemonlist](../index.md)/[PokemonListViewModel](index.md)

# PokemonListViewModel

[common]\
public final class [PokemonListViewModel](index.md) extends BaseViewModel implements KoinComponent

## Constructors

| | |
|---|---|
| [PokemonListViewModel](-pokemon-list-view-model.md) | [common]<br>public [PokemonListViewModel](index.md)[PokemonListViewModel](-pokemon-list-view-model.md)() |

## Functions

| Name | Summary |
|---|---|
| [getAllPokemons](get-all-pokemons.md) | [common]<br>public final MutableStateFlow&lt;[List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;&gt;[getAllPokemons](get-all-pokemons.md)() |
| [getPage](get-page.md) | [common]<br>public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getPage](get-page.md)() |
| [getPokemon](get-pokemon.md) | [common]<br>public final MutableStateFlow&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;[getPokemon](get-pokemon.md)() |
| [getPokemons](get-pokemons.md) | [common]<br>public final MutableStateFlow&lt;[List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;&gt;[getPokemons](get-pokemons.md)() |
| [getPokemonsLocal](get-pokemons-local.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[getPokemonsLocal](get-pokemons-local.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)limit, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)searchArg) |
| [getSearch](get-search.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getSearch](get-search.md)() |
| [isChosen](is-chosen.md) | [common]<br>public final [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[isChosen](is-chosen.md)() |
| [setAllPokemons](set-all-pokemons.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setAllPokemons](set-all-pokemons.md)(MutableStateFlow&lt;[List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;&gt;allPokemons) |
| [setChosen](set-chosen.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setChosen](set-chosen.md)([Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)isChosen) |
| [setPage](set-page.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setPage](set-page.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)page) |
| [setPokemon](set-pokemon.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setPokemon](set-pokemon.md)(MutableStateFlow&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;pokemon) |
| [setPokemons](set-pokemons.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setPokemons](set-pokemons.md)(MutableStateFlow&lt;[List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;&gt;pokemons) |
| [setSearch](set-search.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSearch](set-search.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)search) |

## Properties

| Name | Summary |
|---|---|
| [allPokemons](index.md#-531150634%2FProperties%2F442279350) | [common]<br>private MutableStateFlow&lt;[List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;&gt;[allPokemons](index.md#-531150634%2FProperties%2F442279350) |
| [isChosen](is-chosen.md) | [common]<br>private [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[isChosen](is-chosen.md) |
| [page](index.md#-1465669484%2FProperties%2F442279350) | [common]<br>private [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[page](index.md#-1465669484%2FProperties%2F442279350) |
| [pokemon](index.md#1874107396%2FProperties%2F442279350) | [common]<br>private MutableStateFlow&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;[pokemon](index.md#1874107396%2FProperties%2F442279350) |
| [pokemons](index.md#-465958077%2FProperties%2F442279350) | [common]<br>private MutableStateFlow&lt;[List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;&gt;[pokemons](index.md#-465958077%2FProperties%2F442279350) |
| [search](index.md#-1803281893%2FProperties%2F442279350) | [common]<br>private [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[search](index.md#-1803281893%2FProperties%2F442279350) |

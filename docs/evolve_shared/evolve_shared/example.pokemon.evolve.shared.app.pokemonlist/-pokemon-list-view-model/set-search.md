//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemonlist](../index.md)/[PokemonListViewModel](index.md)/[setSearch](set-search.md)

# setSearch

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSearch](set-search.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)search)

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemonlist](../index.md)/[PokemonListViewModel](index.md)/[getAllPokemons](get-all-pokemons.md)

# getAllPokemons

[common]\

public final MutableStateFlow&lt;[List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;&gt;[getAllPokemons](get-all-pokemons.md)()

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemonlist](../index.md)/[PokemonListViewModel](index.md)/[getPokemonsLocal](get-pokemons-local.md)

# getPokemonsLocal

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[getPokemonsLocal](get-pokemons-local.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)limit, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)searchArg)

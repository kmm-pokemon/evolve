//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared.app.pokemonlist](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [PokemonListFragment](-pokemon-list-fragment/index.md) | [android]<br>public final class [PokemonListFragment](-pokemon-list-fragment/index.md) extends BaseFragment&lt;&lt;Error class: unknown class&gt;&gt; |
| [PokemonListViewModel](-pokemon-list-view-model/index.md) | [common]<br>public final class [PokemonListViewModel](-pokemon-list-view-model/index.md) extends BaseViewModel implements KoinComponent |

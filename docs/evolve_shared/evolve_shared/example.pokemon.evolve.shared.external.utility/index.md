//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared.external.utility](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [GridSpacingItemDecoration](-grid-spacing-item-decoration/index.md) | [android]<br>public final class [GridSpacingItemDecoration](-grid-spacing-item-decoration/index.md) extends [RecyclerView.ItemDecoration](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.ItemDecoration.html) |
| [RealmListPokemonEvolutionSerializer](-realm-list-pokemon-evolution-serializer/index.md) | [common]<br>public final class [RealmListPokemonEvolutionSerializer](-realm-list-pokemon-evolution-serializer/index.md) implements KSerializer&lt;RealmList&lt;[PokemonEvolution](../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)&gt;&gt; |
| [RealmListPokemonStatSerializer](-realm-list-pokemon-stat-serializer/index.md) | [common]<br>public final class [RealmListPokemonStatSerializer](-realm-list-pokemon-stat-serializer/index.md) implements KSerializer&lt;RealmList&lt;[PokemonStat](../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-stat/index.md)&gt;&gt; |
| [RealmListPokemonTypeSerializer](-realm-list-pokemon-type-serializer/index.md) | [common]<br>public final class [RealmListPokemonTypeSerializer](-realm-list-pokemon-type-serializer/index.md) implements KSerializer&lt;RealmList&lt;[PokemonType](../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-type/index.md)&gt;&gt; |
| [ScaledDragShadowBuilder](-scaled-drag-shadow-builder/index.md) | [android]<br>public final class [ScaledDragShadowBuilder](-scaled-drag-shadow-builder/index.md) extends [View.DragShadowBuilder](https://developer.android.com/reference/kotlin/android/view/View.DragShadowBuilder.html) |
| [ZoomImageView](-zoom-image-view/index.md) | [android]<br>public final class [ZoomImageView](-zoom-image-view/index.md) |

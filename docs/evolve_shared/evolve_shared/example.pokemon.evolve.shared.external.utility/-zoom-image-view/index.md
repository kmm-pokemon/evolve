//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[ZoomImageView](index.md)

# ZoomImageView

[android]\
public final class [ZoomImageView](index.md)

## Constructors

| | |
|---|---|
| [ZoomImageView](-zoom-image-view.md) | [android]<br>public [ZoomImageView](index.md)[ZoomImageView](-zoom-image-view.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)imageUrl, [View](https://developer.android.com/reference/kotlin/android/view/View.html)outerContainerView, [View](https://developer.android.com/reference/kotlin/android/view/View.html)innerContainerView, [View](https://developer.android.com/reference/kotlin/android/view/View.html)thumbView, [ImageView](https://developer.android.com/reference/kotlin/android/widget/ImageView.html)targetView, [View](https://developer.android.com/reference/kotlin/android/view/View.html)closeView, Function0&lt;[Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;onClose) |

## Functions

| Name | Summary |
|---|---|
| [hide](hide.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[hide](hide.md)() |
| [show](show.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[show](show.md)() |

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[ZoomImageView](index.md)/[ZoomImageView](-zoom-image-view.md)

# ZoomImageView

[android]\

public [ZoomImageView](index.md)[ZoomImageView](-zoom-image-view.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)imageUrl, [View](https://developer.android.com/reference/kotlin/android/view/View.html)outerContainerView, [View](https://developer.android.com/reference/kotlin/android/view/View.html)innerContainerView, [View](https://developer.android.com/reference/kotlin/android/view/View.html)thumbView, [ImageView](https://developer.android.com/reference/kotlin/android/widget/ImageView.html)targetView, [View](https://developer.android.com/reference/kotlin/android/view/View.html)closeView, Function0&lt;[Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;onClose)

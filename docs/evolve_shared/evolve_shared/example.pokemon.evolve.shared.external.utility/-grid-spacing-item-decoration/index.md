//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[GridSpacingItemDecoration](index.md)

# GridSpacingItemDecoration

[android]\
public final class [GridSpacingItemDecoration](index.md) extends [RecyclerView.ItemDecoration](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.ItemDecoration.html)

## Constructors

| | |
|---|---|
| [GridSpacingItemDecoration](-grid-spacing-item-decoration.md) | [android]<br>public [GridSpacingItemDecoration](index.md)[GridSpacingItemDecoration](-grid-spacing-item-decoration.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)spanCount, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)spacing, [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)includeEdge) |

## Functions

| Name | Summary |
|---|---|
| [getItemOffsets](get-item-offsets.md) | [android]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[getItemOffsets](get-item-offsets.md)([Rect](https://developer.android.com/reference/kotlin/android/graphics/Rect.html)outRect, [View](https://developer.android.com/reference/kotlin/android/view/View.html)view, [RecyclerView](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.html)parent, [RecyclerView.State](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.State.html)state) |

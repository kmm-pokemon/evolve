//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[GridSpacingItemDecoration](index.md)/[getItemOffsets](get-item-offsets.md)

# getItemOffsets

[android]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[getItemOffsets](get-item-offsets.md)([Rect](https://developer.android.com/reference/kotlin/android/graphics/Rect.html)outRect, [View](https://developer.android.com/reference/kotlin/android/view/View.html)view, [RecyclerView](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.html)parent, [RecyclerView.State](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.State.html)state)

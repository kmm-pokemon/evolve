//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonTypeSerializer](index.md)/[RealmListPokemonTypeSerializer](-realm-list-pokemon-type-serializer.md)

# RealmListPokemonTypeSerializer

[common]\

public [RealmListPokemonTypeSerializer](index.md)[RealmListPokemonTypeSerializer](-realm-list-pokemon-type-serializer.md)(KSerializer&lt;[PokemonType](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-type/index.md)&gt;dataSerializer)

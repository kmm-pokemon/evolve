//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonTypeSerializer](index.md)/[getDataSerializer](get-data-serializer.md)

# getDataSerializer

[common]\

public final KSerializer&lt;[PokemonType](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-type/index.md)&gt;[getDataSerializer](get-data-serializer.md)()

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonEvolutionSerializer](index.md)/[RealmListPokemonEvolutionSerializer](-realm-list-pokemon-evolution-serializer.md)

# RealmListPokemonEvolutionSerializer

[common]\

public [RealmListPokemonEvolutionSerializer](index.md)[RealmListPokemonEvolutionSerializer](-realm-list-pokemon-evolution-serializer.md)(KSerializer&lt;[PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)&gt;dataSerializer)

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonEvolutionSerializer](index.md)/[getDataSerializer](get-data-serializer.md)

# getDataSerializer

[common]\

public final KSerializer&lt;[PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)&gt;[getDataSerializer](get-data-serializer.md)()

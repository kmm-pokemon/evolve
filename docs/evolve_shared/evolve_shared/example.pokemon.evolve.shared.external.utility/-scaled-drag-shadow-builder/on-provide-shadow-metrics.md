//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[ScaledDragShadowBuilder](index.md)/[onProvideShadowMetrics](on-provide-shadow-metrics.md)

# onProvideShadowMetrics

[android]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onProvideShadowMetrics](on-provide-shadow-metrics.md)([Point](https://developer.android.com/reference/kotlin/android/graphics/Point.html)outShadowSize, [Point](https://developer.android.com/reference/kotlin/android/graphics/Point.html)outShadowTouchPoint)

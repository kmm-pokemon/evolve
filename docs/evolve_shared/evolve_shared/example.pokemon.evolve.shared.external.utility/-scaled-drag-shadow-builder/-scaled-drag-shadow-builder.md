//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[ScaledDragShadowBuilder](index.md)/[ScaledDragShadowBuilder](-scaled-drag-shadow-builder.md)

# ScaledDragShadowBuilder

[android]\

public [ScaledDragShadowBuilder](index.md)[ScaledDragShadowBuilder](-scaled-drag-shadow-builder.md)([View](https://developer.android.com/reference/kotlin/android/view/View.html)view, [Float](https://developer.android.com/reference/kotlin/java/lang/Float.html)scale)

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonStatSerializer](index.md)/[RealmListPokemonStatSerializer](-realm-list-pokemon-stat-serializer.md)

# RealmListPokemonStatSerializer

[common]\

public [RealmListPokemonStatSerializer](index.md)[RealmListPokemonStatSerializer](-realm-list-pokemon-stat-serializer.md)(KSerializer&lt;[PokemonStat](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-stat/index.md)&gt;dataSerializer)

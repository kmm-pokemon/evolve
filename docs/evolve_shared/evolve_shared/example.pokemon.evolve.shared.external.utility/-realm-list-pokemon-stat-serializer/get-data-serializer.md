//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonStatSerializer](index.md)/[getDataSerializer](get-data-serializer.md)

# getDataSerializer

[common]\

public final KSerializer&lt;[PokemonStat](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-stat/index.md)&gt;[getDataSerializer](get-data-serializer.md)()

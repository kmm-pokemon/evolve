//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonStatSerializer](index.md)/[serialize](serialize.md)

# serialize

[common]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[serialize](serialize.md)(Encoderencoder, RealmList&lt;[PokemonStat](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-stat/index.md)&gt;obj)

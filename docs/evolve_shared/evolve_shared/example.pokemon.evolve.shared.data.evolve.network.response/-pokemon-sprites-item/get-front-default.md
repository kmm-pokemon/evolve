//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpritesItem](index.md)/[getFrontDefault](get-front-default.md)

# getFrontDefault

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getFrontDefault](get-front-default.md)()

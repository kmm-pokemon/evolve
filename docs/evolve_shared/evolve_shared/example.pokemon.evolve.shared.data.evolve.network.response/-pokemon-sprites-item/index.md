//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpritesItem](index.md)

# PokemonSpritesItem

[common]\
@Serializable()

public final class [PokemonSpritesItem](index.md)

## Constructors

| | |
|---|---|
| [PokemonSpritesItem](-pokemon-sprites-item.md) | [common]<br>public [PokemonSpritesItem](index.md)[PokemonSpritesItem](-pokemon-sprites-item.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)default, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)frontDefault) |

## Functions

| Name | Summary |
|---|---|
| [getDefault](get-default.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getDefault](get-default.md)() |
| [getFrontDefault](get-front-default.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getFrontDefault](get-front-default.md)() |
| [setDefault](set-default.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setDefault](set-default.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)default) |
| [setFrontDefault](set-front-default.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setFrontDefault](set-front-default.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)frontDefault) |

## Properties

| Name | Summary |
|---|---|
| [default](index.md#-785724493%2FProperties%2F442279350) | [common]<br>private [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[default](index.md#-785724493%2FProperties%2F442279350) |
| [frontDefault](index.md#-2026907090%2FProperties%2F442279350) | [common]<br>private [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[frontDefault](index.md#-2026907090%2FProperties%2F442279350) |

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpritesItem](index.md)/[setDefault](set-default.md)

# setDefault

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setDefault](set-default.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)default)

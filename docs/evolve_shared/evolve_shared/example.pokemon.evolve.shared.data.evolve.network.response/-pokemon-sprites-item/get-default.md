//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpritesItem](index.md)/[getDefault](get-default.md)

# getDefault

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getDefault](get-default.md)()

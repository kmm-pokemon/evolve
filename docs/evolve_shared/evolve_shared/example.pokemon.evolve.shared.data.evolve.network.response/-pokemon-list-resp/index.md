//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonListResp](index.md)

# PokemonListResp

[common]\
@Serializable()

public final class [PokemonListResp](index.md)

## Constructors

| | |
|---|---|
| [PokemonListResp](-pokemon-list-resp.md) | [common]<br>public [PokemonListResp](index.md)[PokemonListResp](-pokemon-list-resp.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;results) |

## Functions

| Name | Summary |
|---|---|
| [getResults](get-results.md) | [common]<br>public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;[getResults](get-results.md)() |
| [setResults](set-results.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setResults](set-results.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;results) |

## Properties

| Name | Summary |
|---|---|
| [results](index.md#41699391%2FProperties%2F442279350) | [common]<br>private [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;[results](index.md#41699391%2FProperties%2F442279350) |

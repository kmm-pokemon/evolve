//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpeciesResp](index.md)/[PokemonSpeciesResp](-pokemon-species-resp.md)

# PokemonSpeciesResp

[common]\

public [PokemonSpeciesResp](index.md)[PokemonSpeciesResp](-pokemon-species-resp.md)([PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)evolutionChain, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)name)

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpeciesResp](index.md)/[setEvolutionChain](set-evolution-chain.md)

# setEvolutionChain

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setEvolutionChain](set-evolution-chain.md)([PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)evolutionChain)

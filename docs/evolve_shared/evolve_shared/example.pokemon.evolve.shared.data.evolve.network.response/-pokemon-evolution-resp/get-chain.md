//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonEvolutionResp](index.md)/[getChain](get-chain.md)

# getChain

[common]\

public final [PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)[getChain](get-chain.md)()

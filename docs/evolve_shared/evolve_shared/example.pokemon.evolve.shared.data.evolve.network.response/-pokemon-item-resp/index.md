//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonItemResp](index.md)

# PokemonItemResp

[common]\
@Serializable()

public final class [PokemonItemResp](index.md)

## Constructors

| | |
|---|---|
| [PokemonItemResp](-pokemon-item-resp.md) | [common]<br>public [PokemonItemResp](index.md)[PokemonItemResp](-pokemon-item-resp.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)id, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)name, [PokemonSpritesItem](../-pokemon-sprites-item/index.md)sprites) |

## Functions

| Name | Summary |
|---|---|
| [getId](get-id.md) | [common]<br>public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getId](get-id.md)() |
| [getName](get-name.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getName](get-name.md)() |
| [getSprites](get-sprites.md) | [common]<br>public final [PokemonSpritesItem](../-pokemon-sprites-item/index.md)[getSprites](get-sprites.md)() |
| [setId](set-id.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setId](set-id.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)id) |
| [setName](set-name.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setName](set-name.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name) |
| [setSprites](set-sprites.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSprites](set-sprites.md)([PokemonSpritesItem](../-pokemon-sprites-item/index.md)sprites) |

## Properties

| Name | Summary |
|---|---|
| [id](index.md#-1682760001%2FProperties%2F442279350) | [common]<br>private [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[id](index.md#-1682760001%2FProperties%2F442279350) |
| [name](index.md#-1323387889%2FProperties%2F442279350) | [common]<br>private [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[name](index.md#-1323387889%2FProperties%2F442279350) |
| [sprites](index.md#472058770%2FProperties%2F442279350) | [common]<br>private [PokemonSpritesItem](../-pokemon-sprites-item/index.md)[sprites](index.md#472058770%2FProperties%2F442279350) |

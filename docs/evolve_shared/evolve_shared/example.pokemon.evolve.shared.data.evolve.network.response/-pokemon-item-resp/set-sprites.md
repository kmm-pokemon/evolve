//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonItemResp](index.md)/[setSprites](set-sprites.md)

# setSprites

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSprites](set-sprites.md)([PokemonSpritesItem](../-pokemon-sprites-item/index.md)sprites)

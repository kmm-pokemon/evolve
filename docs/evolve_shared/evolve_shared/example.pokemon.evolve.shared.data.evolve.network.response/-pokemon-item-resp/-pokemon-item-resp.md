//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonItemResp](index.md)/[PokemonItemResp](-pokemon-item-resp.md)

# PokemonItemResp

[common]\

public [PokemonItemResp](index.md)[PokemonItemResp](-pokemon-item-resp.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)id, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)name, [PokemonSpritesItem](../-pokemon-sprites-item/index.md)sprites)

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[BerryListResp](index.md)/[getResults](get-results.md)

# getResults

[common]\

public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;[getResults](get-results.md)()

//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpritesOther](index.md)

# PokemonSpritesOther

[common]\
@Serializable()

public final class [PokemonSpritesOther](index.md)

## Constructors

| | |
|---|---|
| [PokemonSpritesOther](-pokemon-sprites-other.md) | [common]<br>public [PokemonSpritesOther](index.md)[PokemonSpritesOther](-pokemon-sprites-other.md)([PokemonSpritesItem](../-pokemon-sprites-item/index.md)officialArtwork) |

## Functions

| Name | Summary |
|---|---|
| [getOfficialArtwork](get-official-artwork.md) | [common]<br>public final [PokemonSpritesItem](../-pokemon-sprites-item/index.md)[getOfficialArtwork](get-official-artwork.md)() |
| [setOfficialArtwork](set-official-artwork.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setOfficialArtwork](set-official-artwork.md)([PokemonSpritesItem](../-pokemon-sprites-item/index.md)officialArtwork) |

## Properties

| Name | Summary |
|---|---|
| [officialArtwork](index.md#-170400712%2FProperties%2F442279350) | [common]<br>private [PokemonSpritesItem](../-pokemon-sprites-item/index.md)[officialArtwork](index.md#-170400712%2FProperties%2F442279350) |

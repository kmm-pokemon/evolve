//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpritesResp](index.md)/[setOther](set-other.md)

# setOther

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setOther](set-other.md)([PokemonSpritesOther](../-pokemon-sprites-other/index.md)other)

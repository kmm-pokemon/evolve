//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.deletedialog](../index.md)/[DeleteDialogFragment](index.md)

# DeleteDialogFragment

[android]\
public final class [DeleteDialogFragment](index.md) extends BaseDialogFragment&lt;&lt;Error class: unknown class&gt;&gt;

## Constructors

| | |
|---|---|
| [DeleteDialogFragment](-delete-dialog-fragment.md) | [android]<br>public [DeleteDialogFragment](index.md)[DeleteDialogFragment](-delete-dialog-fragment.md)() |

## Functions

| Name | Summary |
|---|---|
| [isCancelable](is-cancelable.md) | [android]<br>public [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[isCancelable](is-cancelable.md)() |
| [onViewCreated](on-view-created.md) | [android]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onViewCreated](on-view-created.md)([View](https://developer.android.com/reference/kotlin/android/view/View.html)view, [Bundle](https://developer.android.com/reference/kotlin/android/os/Bundle.html)savedInstanceState) |

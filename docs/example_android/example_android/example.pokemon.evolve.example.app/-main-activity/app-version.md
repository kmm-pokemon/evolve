//[example_android](../../../index.md)/[example.pokemon.evolve.example.app](../index.md)/[MainActivity](index.md)/[appVersion](app-version.md)

# appVersion

[androidJvm]\

public [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[appVersion](app-version.md)()

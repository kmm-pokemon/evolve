package example.pokemon.evolve.example.app

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import example.pokemon.core.shared.app.common.BaseFragment
import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.evolve.example.R
import example.pokemon.evolve.example.databinding.MainFragmentBinding
import example.pokemon.evolve.shared.deleteRealm
import kotlinx.coroutines.launch

class ExampleFragment: BaseFragment<MainFragmentBinding>(R.layout.main_fragment) {
    override fun showActionBar() = false
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.mbNewGame.setOnClickListener {
            start(true)
        }
        binding.mbContinue.setOnClickListener {
            start()
        }
    }

    private fun start(clear: Boolean = false) {
        viewLifecycleOwner.lifecycleScope.launch {
            showFullLoading(true)
            if (clear) {
                sharedPreferences.edit().remove(AppConstant.ONBOARDING_KEY).remove(AppConstant.EVOLVING_KEY).apply()
                deleteRealm()
            }
            showFullLoading(false)

            (requireActivity() as MainActivity).isStarted = true
            findNavController().graph.clear()
            findNavController().setGraph((requireActivity() as MainActivity).navGraph())
        }
    }
}
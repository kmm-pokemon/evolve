package example.pokemon.evolve.example.app

import android.content.SharedPreferences
import example.pokemon.core.shared.app.common.BaseActivity
import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.evolve.example.R
import org.koin.core.component.inject
import example.pokemon.evolve.shared.R as eR

class MainActivity : BaseActivity() {
    private val sharedPreferences: SharedPreferences by inject()
    var isStarted = false

    override fun appVersion() = getString(R.string.app_version)
    override fun navGraph(): Int {

        val evolving = sharedPreferences.getString(AppConstant.EVOLVING_KEY, "")
        return if (!evolving.isNullOrEmpty()) {
            isStarted = true
            eR.navigation.detail_nav_graph
        } else if (!isStarted) R.navigation.example_nav_graph
        else {
            val onBoarding = sharedPreferences.getBoolean(AppConstant.ONBOARDING_KEY, false)
            if (onBoarding) eR.navigation.evolve_nav_graph
            else eR.navigation.onboarding_nav_graph
        }
    }

    override fun topLevelDestinations(): Set<Int> {
        val list = HashSet<Int>()
        list.add(eR.id.exploreFragment)
        list.add(eR.id.collectionsFragment)
        return list
    }
}
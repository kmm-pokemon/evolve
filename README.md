# Traveler Evolve

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![pipeline status](https://gitlab.com/kmm-pokemon/evolve/badges/main/pipeline.svg)](https://gitlab.com/kmm-pokemon/evolve/-/commits/main) [![coverage report](https://gitlab.com/kmm-pokemon/evolve/badges/main/coverage.svg)](https://gitlab.com/kmm-pokemon/evolve/-/commits/main) [![Latest Release](https://gitlab.com/kmm-pokemon/evolve/-/badges/release.svg)](https://gitlab.com/kmm-pokemon/evolve/-/releases)

## Contents

- [Documentation](https://gitlab.com/kmm-pokemon/evolve/tree/main/docs)
- [Features](#features)
- [Demo](#demo)
- [Requirements](#requirements)
- [Project Structure](#project-structure)
- [Architecture](#architecture)
- [Commands](#commands)

## Features

- Provide onboarding story with 2 mode
    - Single to evolve 1 pokemon
    - Multiple to evolve multiple pokemon
- Provide explore pokemon screen with search autocomplete by name
- Provide see detail pokemon with animation
- Provide list saved collections pokemon
- Provide detail of pokemon:
    - Drag and drop berry to feed pokemon
    - Animated progress to evolve
    - Shining animation after progress fully achieved
    - Evolve pokemon
    - Delete pokemon with dialog confirmation
- Enriched with sound effects and vibrate gesture
- Powered by KMM with KOIN for dependency injection and using MVVM pattern with clean architecture.

## Demo

### Android

![Demo Single Evolve](https://gitlab.com/kmm-pokemon/evolve/-/raw/main/resources/demo_android_single.gif){width=250px}
![Demo Multiple Evolve](https://gitlab.com/kmm-pokemon/evolve/-/raw/main/resources/demo_android_multiple.gif){width=250px}

To install this example app on Android, you can join as Tester on this [Firebase Distribution Invitation Link](https://appdistribution.firebase.dev/i/dc55d903fc28931b).
Or build on your local with follow the project [Requirements](#requirements)

### IOS

![Demo Single Evolve](https://gitlab.com/kmm-pokemon/evolve/-/raw/main/resources/demo_ios_single.gif){width=250px}
![Demo Multiple Evolve](https://gitlab.com/kmm-pokemon/evolve/-/raw/main/resources/demo_ios_multiple.gif){width=250px}

To install this example app on iOS, please build and run on your local, with following:
- Complete the project [Requirements](#requirements)
- Open terminal, change directory to this `example_ios` then pod install:

```sh
~%cd example_ios
~%pod install
```
- Open XCode, then `File -> Open (browse to example_ios)`
- Choose Target installation (Real Device / iOS Simulator)
- Run program on `Product -> Run`

## Requirements

1. Minimum Android/SDK Version: Nougat/23
2. Deployment Target iOS/SDK Version: 14.1
3. Target Android/SDK Version: Snow Cone/32
4. Compile Android/SDK Version: Snow Cone/32
5. This project is built using Android Studio version 2022.1.1 and Android Gradle 7.5
6. For iOS, please install [COCOAPODS](https://cocoapods.org/)
7. This android project using google service which need `google-services.json`, with following:
>- Create new project [Firebase](https://console.firebase.google.com/) on your account
>- Add `Android App` fill `Package name` with example.pokemon.evolve.example for run **Production** flavour
>- Add `Android App` fill `Package name` with example.pokemon.evolve.example.beta for run **Beta** flavour
>- Add `Android App` fill `Package name` with example.pokemon.evolve.example.stage for run **Stage** flavour
>- Add `Android App` fill `Package name` with example.pokemon.evolve.example.dev for run **Development** flavour
>- Download `google-services.json` paste to `example_android` folder
8. Create `properties.gradle` in your root folder, add this content:
```groovy
ext {
    gitlab = [
            publishToken: "$publishToken",
            consumeToken: "$consumeToken"
    ]

    deeplink = "android-app://example.app.id"

    server = [
            dev  : "\"pokeapi.co\"",
            stage: "\"pokeapi.co\"",
            beta : "\"pokeapi.co\"",
            prod : "\"pokeapi.co\"",
    ]
}
```
> ***note:***
>- replace ***$publishToken*** with your private token if you forked to your private project, otherwise leave it blank if only wanted to run.
>- replace ***$consumeToken*** with your private token if you forked to your private project, otherwise leave it blank if only wanted to run.

## Project Structure

```plantuml
:evolve_shared;
fork
    :example_android;
fork again
    :evolve_ios;
    :example_ios;
end fork
end
```
For the high level hierarchy, the project separate into 4 main modules, which are :

### 1. [Example Android](https://gitlab.com/kmm-pokemon/evolve/tree/main/example_android)

This module contains the android application's example usage of this project.

### 2. [Example iOS](https://gitlab.com/kmm-pokemon/evolve/tree/main/example_ios)

This module contains the iOS application's example usage of this project.

### 3. [Evolve iOS](https://gitlab.com/kmm-pokemon/evolve/tree/main/evolve_ios)
This module contains iOS code that holds the iOS library, that can be injected to iOS app.

### 4. [Evolve Shared](https://gitlab.com/kmm-pokemon/evolve/tree/main/evolve_shared)
This module contains shared code that holds the domain and data layers and some part of the presentation logic ie.shared viewmodels.

## Architecture

This project implement
Clean [Architecture by Fernando Cejas](https://github.com/android10/Android-CleanArchitecture)

### Clean architecture

![Image Clean architecture](https://gitlab.com/kmm-pokemon/evolve/-/raw/main/resources/clean_architecture.png)

### Architectural approach

![Image Architectural approach](https://gitlab.com/kmm-pokemon/evolve/-/raw/main/resources/clean_architecture_layers.png)

### Architectural reactive approach

![Image Architectural reactive approach](https://gitlab.com/kmm-pokemon/evolve/-/raw/main/resources/clean_architecture_layers_details.png)

## Commands

Here are some useful gradle/adb commands for executing this example:

* ./gradlew clean build - Build the entire project and execute unit tests
* ./gradlew clean sonarqube - Execute sonarqube coverage report
* ./gradlew dokkaGfm - Generate documentation
* ./gradlew test[flavor][buildType]UnitTest - Execute unit tests e.g., testDevDebugUnitTest
* ./gradlew test[flavor][buildType]UnitTest create[flavor][buildType]CoverageReport - Execute unit
  tests and create coverage report e.g., createDevDebugCoverageReport
* ./gradlew assemble[flavor][buildType] - Create apk file e.g., assembleDevDebug
* ./gradlew :evolve_shared:assembleXCFramework - Generate XCFramework for iOS
* ./gradlew publish - Publish - Publish to repository packages (MAVEN)

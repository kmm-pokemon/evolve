import UIKit
import core
import evolve_shared

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    let koinApp = EvolveKoinKt.doInitKoin(
        context: NSObject(),
        host: Bundle.main.infoDictionary!["HOST"] as! String,
        deviceId: UIDevice.current.identifierForVendor?.uuidString ?? "ios-${random()}",
        version: AppInfo.appVersion
    )
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }

    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}

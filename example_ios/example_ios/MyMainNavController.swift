import UIKit
import core
import core_shared
import evolve_shared

class MyMainNavController: UIViewController {
    @IBOutlet weak var newGameButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    
    @IBAction func newGameAction(_ sender: Any) {
        Preferences.value(forKey: AppConstant.shared.ONBOARDING_KEY, value: false)
        Preferences.value(forKey: AppConstant.shared.EVOLVING_KEY, value: "")
        EvolveKoinKt.deleteRealm()
        performSegue(withIdentifier: "MyMainToMainNav", sender: nil)
    }
    
    @IBAction func continueAction(_ sender: Any) {
        performSegue(withIdentifier: "MyMainToMainNav", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        continueButton.tintColor = .primary
        newGameButton.tintColor = .primary
        newGameButton.layer.cornerRadius = newGameButton.bounds.height / 2
        if #available(iOS 15.0, *) {
            var config = UIButton.Configuration.filled()
            config.cornerStyle = .capsule
            newGameButton.configuration = config
        } else {
            // TODO: set new game button style
        }
    }
}

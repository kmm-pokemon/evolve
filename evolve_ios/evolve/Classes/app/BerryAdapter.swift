import UIKit
import evolve_shared

class BerryAdapter: UICollectionView {
    public var items = [Berry]()
    public var itemSize = CGSize(width: 45, height: 45)
    public var spacing = CGFloat(4)

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.register(BerryItemController.nib(), forCellWithReuseIdentifier: BerryItemController.identifier)
        self.delegate = self
        self.dataSource = self
    }
}

// MARK: - DataSource
extension BerryAdapter: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfSections section: Int) -> Int {
        return 1
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BerryItemController.identifier, for: indexPath) as! BerryItemController
        cell.configure(with: items[indexPath.row])
        return cell
    }
}

// MARK: - DelegateFlowLayout
extension BerryAdapter: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return itemSize
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return spacing
    }
}

// MARK: - Action
extension BerryAdapter {
    public func clear() {
        var indexes = [IndexPath]()
        for (i,_) in items.enumerated() {
            indexes.append(IndexPath(row: i, section: 0))
        }
        items.removeAll()
        UIView.performWithoutAnimation {
            deleteItems(at: indexes)
        }
        showShimmer(false)
    }
}

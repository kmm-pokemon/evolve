import UIKit
import core
import core_shared
import evolve_shared
import Toaster

class CollectionNavController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pokemonAdapter: PokemonAdapter!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var emptyInfoView: UILabel!
    
    private let inset = CGFloat(12)
    private let minimumLineSpacing = CGFloat(12)
    private let minimumInteritemSpacing = CGFloat(12)
    private let cellsPerRow = CGFloat(2)
    private var vm = PokemonListViewModel()
    private var vmDetail = PokemonDetailViewModel()
    private var refreshControl: UIRefreshControl!
    private var debounce: Timer?
    private var initialized = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.sizeToFit()
        setupPokemonsAdapter()
        setupObservable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        
        scrollView.refreshControl = refreshControl
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.isScrollEnabled = true
        scrollView.scrollToTop()
        
        searchBar.delegate = self
        searchBar.autocapitalizationType = .none
        searchBar.text = ""
        vm.search = ""
        
        pokemonAdapter.isScrollEnabled = false
        
        if !initialized {
            vm.isChosen = true
            load(0, "view will appear", 100000)
            initialized = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        pokemonAdapter.clear()
        initialized = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CollectionToDetailNav" {
          if let vc = segue.destination as? DetailNavController {
              vc.nameArg = sender as? String
          }
        }
    }
    
    @objc func onRefresh() {
        load(0, "pull to refresh")
    }
    
    private func load(_ p: Int32, _ f: String, _ limit: Int32 = AppConstant.shared.LIST_LIMIT) {
        print("load: \(p) from \(f)")
        view.endEditing(true)
        vm.page = p
        if p == 0 {
            pokemonAdapter.clear()
        }
        vm.getPokemonsLocal(limit: limit, searchArg: nil)
    }
}

// MARK: - ScrollViewDelegate
extension CollectionNavController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height) {
            scrollView.isScrollEnabled = false
            pokemonAdapter.isScrollEnabled = true
        }
    }
}

// MARK: - SearchBarDelegate
extension CollectionNavController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        debounce?.invalidate()
        debounce = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { _ in
            self.vm.search = searchText
            self.load(0, "search", searchText.isEmpty ? AppConstant.shared.LIST_LIMIT : 1)
        }
    }
}

// MARK: - Action
extension CollectionNavController {
    func setupPokemonsAdapter() {
        let marginsAndInsets = inset * 2 + minimumInteritemSpacing * (cellsPerRow - 1)
        let itemWidth = ((pokemonAdapter.bounds.size.width - marginsAndInsets) / cellsPerRow).rounded(.down)
        pokemonAdapter.itemSize = CGSize(width: itemWidth, height: itemWidth)
        pokemonAdapter.shadowOpacity = 0.1
        pokemonAdapter.enableFilter = false
        pokemonAdapter.fetchData = {
            self.vm.page += 1
            self.load(self.vm.page, "reach bottom")
        }
        pokemonAdapter.onScrolled = { scrollview in
            if scrollview.contentOffset.y <= 0 && self.pokemonAdapter.isScrollEnabled {
                self.scrollView.isScrollEnabled = true
                self.pokemonAdapter.isScrollEnabled = false
            }
        }
        pokemonAdapter.onSelected = { item in
            guard let n = item.name else { return }
            self.performSegue(withIdentifier: "CollectionToDetailNav", sender: n)
        }
        let skl = PokemonUrl()
        skl.name = ""
        pokemonAdapter.skeleton = skl
    }
    
    func setupObservable() {
        vm.loadingIndicator.collect(collector: Collector<Bool> { isLoading in
            self.onLoading(isLoading)
        }){ e in }
        vm.successMessage.collect(collector: Collector<String> { message in
            if let m = message { Toast.showSuccess(m) }
            self.vm.successMessage.setValue(nil)
        }){ e in }
        vm.errorMessage.collect(collector: Collector<String> { message in
            if let m = message { Toast.showError(m) }
            self.vm.errorMessage.setValue(nil)
        }){ e in }
        vm.allPokemons.collect(collector: Collector<[PokemonUrl]> { pokemons in
            if let its = pokemons, !its.isEmpty {
                self.load(0, "after get all pokemon")
            }
            self.vm.allPokemons.setValue(nil)
        }){ e in }
        vm.pokemons.collect(collector: Collector<[PokemonUrl]> { pokemons in
            if let its = pokemons {
                self.pokemonAdapter.addItems(its.count) {
                    for it in its {
                        self.pokemonAdapter.items.append(it)
                        self.pokemonAdapter.itemsCache.append(it)
                    }
                }
                self.emptyInfoView.isHidden = self.pokemonAdapter.items.count != 0
            }
            self.vm.pokemons.setValue(nil)
        }){ e in }
    }

    func onLoading(_ isLoading: Bool?) {
        if let showLoading = isLoading {
            if pokemonAdapter.itemsCache.count == 0 && showLoading {
                pokemonAdapter.showSkeletons()
            } else {
                refreshControl.endRefreshing()
            }
        }
    }
}

import UIKit
import evolve_shared

class PokemonStatItemController: UICollectionViewCell {
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var subtitleView: UILabel!
    
    static let identifier = "PokemonStatItemView"
    static func nib() -> UINib {
        return UINib(nibName: "PokemonStatItemView", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(with item: PokemonStat) {
        if let n = item.stat?.name {
            titleView.text = n.replacingOccurrences(of: "-", with: " ").capitalized
        }
        if let s = item.baseStat {
            subtitleView.text = String(Int(truncating: s))
        }
    }

}

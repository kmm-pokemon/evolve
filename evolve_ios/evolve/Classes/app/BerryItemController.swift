import UIKit
import evolve_shared

class BerryItemController: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    static let identifier = "BerryItemView"
    static func nib() -> UINib {
        return UINib(nibName: "BerryItemView", bundle: nil)
    }
    private var berry: Berry!

    override func awakeFromNib() {
        super.awakeFromNib()
        let dragInteraction = UIDragInteraction(delegate: self)
        dragInteraction.isEnabled = true
        self.addInteraction(dragInteraction)
    }
    
    public func configure(with item: Berry) {
        berry = item
        if let i = item.image {
            imageView?.sd_setImage(with: URL(string: i))
        }
    }

}

//MARK: DragDelegate
extension BerryItemController: UIDragInteractionDelegate {
    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
        guard let n = berry.name else { return [] }
        let provider = NSItemProvider(object: n as NSString)
        let item = UIDragItem(itemProvider: provider)
        item.localObject = n
        return [item]
    }
}

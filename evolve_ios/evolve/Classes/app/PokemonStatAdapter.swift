import UIKit
import evolve_shared

class PokemonStatAdapter: UICollectionView {
    public var items = [PokemonStat]()
    public var itemSize = CGSize(width: 0, height: 0)
    public var spacing = CGFloat(0)

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.register(PokemonStatItemController.nib(), forCellWithReuseIdentifier: PokemonStatItemController.identifier)
        self.delegate = self
        self.dataSource = self
    }
}

// MARK: - DataSource
extension PokemonStatAdapter: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfSections section: Int) -> Int {
        return 1
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PokemonStatItemController.identifier, for: indexPath) as! PokemonStatItemController
        cell.configure(with: items[indexPath.row])
        return cell
    }
}

// MARK: - DelegateFlowLayout
extension PokemonStatAdapter: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return itemSize
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacing
    }
}

// MARK: - Action
extension PokemonStatAdapter {
    public func clear() {
        var indexes = [IndexPath]()
        for (i,_) in items.enumerated() {
            indexes.append(IndexPath(row: i, section: 0))
        }
        items.removeAll()
        UIView.performWithoutAnimation {
            deleteItems(at: indexes)
        }
        showShimmer(false)
    }
}

import UIKit
import evolve_shared
import SDWebImage

class PokemonItemController: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelView: UILabel!
    
    static let identifier = "PokemonItemView"
    static func nib() -> UINib {
        return UINib(nibName: "PokemonItemView", bundle: nil)
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(with item: PokemonUrl, enableFilter: Bool) {
        labelView?.isHidden = true
        labelView?.backgroundColor = .primary
        labelView?.textColor = .white
        if let o = item.order {
            print("\nconfigured item: \(o)\n")
            labelView?.text = "  #\(Int(truncating: o)+1)  "
            labelView?.isHidden = false
        }
    
        imageView?.image = UIImage(named: "image_404")
        imageView?.backgroundColor = .grey20
        imageView?.contentMode = .scaleAspectFill
        if let p = item.pokemon {
            guard let i = p.image else { return }
            imageView?.backgroundColor = .clear
            imageView?.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imageView?.sd_setImage(with: URL(string: i), completed: {_,_,_,_ in
                if let c = item.isChosen, c == true && enableFilter {
                    let tintedImage = self.imageView?.image?.withRenderingMode(.alwaysTemplate)
                    self.imageView?.image = tintedImage
                    self.imageView?.tintColor = .grey20
                }
            })
        }
    }

}

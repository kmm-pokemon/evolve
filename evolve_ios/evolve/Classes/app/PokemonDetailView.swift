import UIKit
import evolve_shared
import SDWebImage

class PokemonDetailView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet var nextTitle: UILabel!
    @IBOutlet var nextNameLabel: UILabel!
    @IBOutlet var progressView: UIProgressView!
    @IBOutlet var progressInfoLabel: UILabel!
    @IBOutlet weak var primaryButton: UIButton!
    @IBOutlet weak var secondaryButton: UIButton!
    @IBOutlet weak var statAdapter: PokemonStatAdapter!
    @IBOutlet var nextContainer: UIView!
    @IBOutlet weak var berryContainer: UIStackView!
    @IBOutlet weak var berryAdapter: BerryAdapter!
    @IBOutlet weak var prevBerryButton: UIButton!
    @IBOutlet weak var nextBerryButton: UIButton!
    
    @IBAction func prevBerryAction(_ sender: Any) {
        let path = berryAdapter.indexPathsForVisibleItems.first
        guard let p = path else { return }
        print("\ndebug-prevBerry: \(p.row)\n")
        berryAdapter.scrollToItem(at: IndexPath(item: p.row - 1, section: 0), at: .bottom, animated: true)
    }
    @IBAction func nextBerryAction(_ sender: Any) {
        let path = berryAdapter.indexPathsForVisibleItems.last
        guard let p = path else { return }
        print("\ndebug-nextBerry: \(p.row)\n")
        berryAdapter.scrollToItem(at: IndexPath(item: p.row + 1, section: 0), at: .top, animated: true)
    }
    
    @IBAction func primaryAction(_ sender: Any) {
        if let action = onPrimaryAction {
            action()
        }
    }
    
    @IBAction func secondaryAction(_ sender: Any) {
        if let action = onSecondaryAction {
            action()
        }
    }
    
    var initialWithBerry = false
    var initialWithPrimaryButton = false
    var onDroppedBerry: ((String) -> Void)? = nil
    private var currentWeight = 0
    private var onPrimaryAction: (() -> Void)? = nil
    private var onSecondaryAction: (() -> Void)? = nil
    
    static let identifier = "PokemonDetailView"
    static func nib() -> UINib {
        return UINib(nibName: "PokemonDetailView", bundle: nil)
    }
    
    override init (frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }

    func initSubviews() {
        PokemonDetailView.nib().instantiate(withOwner: self, options: nil)
        progressView.tintColor = .primary
        
        let itemWidth = ((statAdapter.bounds.size.width - CGFloat(50)) / 2).rounded(.down)
        statAdapter.itemSize = CGSize(width: itemWidth, height: 55)
        statAdapter.spacing = CGFloat(4)
        
        setupBerry()
        setupAction()
        subviews.forEach { $0.removeFromSuperview() }
        addSubview(contentView)
    }
    
    private func setupBerry() {
        if !initialWithBerry {
            berryContainer.removeFromSuperview()
        } else {
            berryAdapter.layer.borderColor = UIColor.lightGray.cgColor
            berryAdapter.layer.borderWidth = 1
            berryAdapter.layer.masksToBounds = true
            berryAdapter.layer.cornerRadius = berryAdapter.bounds.width / 2
            
            prevBerryButton.tintColor = .primary
            nextBerryButton.tintColor = .primary
            berryAdapter.dragInteractionEnabled = true
            let dropInteraction = UIDropInteraction(delegate: self)
            contentView.addInteraction(dropInteraction)
        }
    }
    
    private func setupAction() {
        primaryButton.isHidden = !initialWithPrimaryButton
        primaryButton.tintColor = .primary
        primaryButton.layer.cornerRadius = primaryButton.bounds.height / 2
        if #available(iOS 15.0, *) {
            var config = UIButton.Configuration.filled()
            config.cornerStyle = .capsule
            primaryButton.configuration = config
        } else {
            // TODO: set primary button style
        }
        
        if #available(iOS 15.0, *) {
            secondaryButton.configuration = .plain()
        } else {
            secondaryButton.titleLabel?.removeFromSuperview()
        }
        secondaryButton.tintColor = .red
        secondaryButton.layer.borderColor = UIColor.red.cgColor
        secondaryButton.layer.borderWidth = 1
        secondaryButton.layer.masksToBounds = true
        secondaryButton.layer.cornerRadius = secondaryButton.bounds.height / 2
        secondaryButton.setImage(UIImage(systemName: initialWithPrimaryButton ? "arrow.down" : "trash"), for: .normal)
    }
}

// MARK: DropInteraction
extension PokemonDetailView: UIDropInteractionDelegate {
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return true
    }

    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        let dropLocation = session.location(in: contentView)
        imageView.alpha = 1
        let operation: UIDropOperation
        if imageView.frame.contains(dropLocation) {
            imageView.alpha = 0.7
            operation = session.localDragSession == nil ? .copy : .move
        } else {
           operation = .cancel
        }
        return UIDropProposal(operation: operation)
    }

    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        let _ = session.loadObjects(ofClass: String.self) { berryName in
            print("success drop \(berryName)")
            guard let action = self.onDroppedBerry else { return }
            action(berryName[0])
        }
        self.imageView.alpha = 1
        
    }
}

// MARK: Actions
extension PokemonDetailView {
    func configure(item: Pokemon, next: String?, onPrimaryAction: (() -> Void)?, onSecondaryAction: (() -> Void)?) {
        if next == nil {
            nextContainer.removeFromSuperview()
            primaryButton.isHidden = true
        }
        
        if let ss = item.stats {
            statAdapter.items = (ss as! [PokemonStat])
            statAdapter.reloadData()
        }
        
        progressView?.setProgress(0, animated: false)
        
        if let i = item.image {
            imageView?.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imageView?.sd_setImage(with: URL(string: i))
        }
        if let n = item.name {
            nameLabel.text = n.capitalized
        }
        if let w = item.weight {
            currentWeight = Int(truncating: w)
        }
        self.onPrimaryAction = onPrimaryAction
        self.onSecondaryAction = onSecondaryAction
    }
    
    func setEvolution(item: Pokemon) {
        if let n = item.name {
            nextNameLabel.text = n.capitalized
        }
        if let w = item.weight {
            progressInfoLabel.text = "(\(currentWeight) / \(w)) Kg"
            let progress = Float(currentWeight) / Float(truncating: w)
            progressView.setProgress(progress, animated: true)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.imageView.showShimmerFromBottom(self.currentWeight >= Int(truncating: w))
            }
            if !initialWithPrimaryButton {
                primaryButton.isHidden = currentWeight < Int(truncating: w)
            }
            primaryButton.titleLabel?.text = currentWeight >= Int(truncating: w) ? "Evolve" : "I Choose You"
        }
    }
    
    func setBerries(_ berries: [Berry]) {
        berryAdapter.items = berries
        berryAdapter.reloadData()
    }
}

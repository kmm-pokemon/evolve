import UIKit
import core
import core_shared
import evolve_shared
import SwiftySound
import Toaster

class HomeNavController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pokemonAdapter: PokemonAdapter!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var detailContainerView: PokemonDetailView!
    @IBOutlet weak var emptyInfoView: UILabel!
    
    private let inset = CGFloat(10)
    private let minimumLineSpacing = CGFloat(10)
    private let minimumInteritemSpacing = CGFloat(10)
    private let cellsPerRow = CGFloat(3)
    private var vm = PokemonListViewModel()
    private var vmDetail = PokemonDetailViewModel()
    private var refreshControl: UIRefreshControl!
    private var selected = ""
    private var nextPokemonName: String? = nil
    private var debounce: Timer?
    private var initialized = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.sizeToFit()
        detailContainerView.initialWithPrimaryButton = true
        setupPokemonsAdapter()
        setupObservable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        
        scrollView.refreshControl = refreshControl
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.isScrollEnabled = true
        scrollView.scrollToTop()
        
        searchBar.delegate = self
        searchBar.autocapitalizationType = .none
        searchBar.text = ""
        vm.search = ""
        
        subTitleLabel.textColor = .title
        pokemonAdapter.isScrollEnabled = false
        detailContainerView.superview?.bringSubviewToFront(detailContainerView)
        
        if !initialized {
            load(0, "view will appear", 100000)
            initialized = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        pokemonAdapter.clear()
        initialized = false
    }
    
    @objc func onRefresh() {
        load(0, "pull to refresh")
    }
    
    private func load(_ p: Int32, _ f: String, _ limit: Int32 = AppConstant.shared.LIST_LIMIT) {
        print("load: \(p) from \(f)")
        view.endEditing(true)
        vm.page = p
        if p == 0 {
            pokemonAdapter.clear()
        }
        vm.getPokemonsLocal(limit: limit, searchArg: nil)
    }
}

// MARK: - ScrollViewDelegate
extension HomeNavController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height) {
            scrollView.isScrollEnabled = false
            pokemonAdapter.isScrollEnabled = true
        }
    }
}

// MARK: - SearchBarDelegate
extension HomeNavController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        debounce?.invalidate()
        debounce = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { _ in
            self.vm.search = searchText
            self.load(0, "search", searchText.isEmpty ? AppConstant.shared.LIST_LIMIT : 1)
        }
    }
}

// MARK: - Action
extension HomeNavController {
    func setupPokemonsAdapter() {
        let marginsAndInsets = inset * 2 + minimumInteritemSpacing * (cellsPerRow - 1)
        let itemWidth = ((pokemonAdapter.bounds.size.width - marginsAndInsets) / cellsPerRow).rounded(.down)
        pokemonAdapter.itemSize = CGSize(width: itemWidth, height: itemWidth)
        pokemonAdapter.shadowOpacity = 0.1
        pokemonAdapter.fetchData = {
            self.vm.page += 1
            self.load(self.vm.page, "reach bottom")
        }
        pokemonAdapter.onScrolled = { scrollView in
            if scrollView.contentOffset.y <= 0 && self.pokemonAdapter.isScrollEnabled {
                self.scrollView.isScrollEnabled = true
                self.pokemonAdapter.isScrollEnabled = false
            }
        }
        pokemonAdapter.onSelected = { item in
            self.selected = item.name!
            self.vmDetail.name = item.name!
            self.vmDetail.pokemon.setValue(nil)
            if item.pokemon == nil {
                self.vmDetail.getPokemonDetailLocal(nameArg: item.name!)
            } else {
                self.vmDetail.pokemon.setValue(item.pokemon)
            }
        }
        let skl = PokemonUrl()
        skl.name = ""
        pokemonAdapter.skeleton = skl
    }
    
    func setupObservable() {
        vm.loadingIndicator.collect(collector: Collector<Bool> { isLoading in
            self.onLoading(isLoading)
        }){ e in }
        vm.successMessage.collect(collector: Collector<String> { message in
            print("\ndebug-successMessage: \(String(describing: message))\n")
            if let m = message { Toast.showSuccess(m) }
            self.vm.successMessage.setValue(nil)
        }){ e in }
        vm.errorMessage.collect(collector: Collector<String> { message in
            print("\ndebug-errorMessage: \(String(describing: message))\n")
            if let m = message {Toast.showError(m) }
            self.vm.errorMessage.setValue(nil)
        }){ e in }
        vm.allPokemons.collect(collector: Collector<[PokemonUrl]> { pokemons in
            if let its = pokemons, !its.isEmpty {
                self.load(0, "after get all pokemon")
            }
            self.vm.allPokemons.setValue(nil)
        }){ e in }
        vm.pokemons.collect(collector: Collector<[PokemonUrl]> { pokemonUrls in
            guard let its = pokemonUrls else { return }
            self.notifyAdapter(its)
            self.vm.pokemons.setValue(nil)
        }){ e in }
        vm.pokemon.collect(collector: Collector<PokemonUrl> { p in
            self.notifyItemAdapter(p)
            self.vm.pokemon.setValue(nil)
        }){ e in }
        
        vmDetail.successMessage.collect(collector: Collector<String> { message in
            print("\ndebug-successMessage: \(String(describing: message))\n")
            if let m = message { Toast.showSuccess(m) }
            self.vmDetail.successMessage.setValue(nil)
        }){ e in }
        vmDetail.errorMessage.collect(collector: Collector<String> { message in
            print("\ndebug-errorMessage: \(String(describing: message))\n")
            if let m = message {Toast.showError(m) }
            self.vmDetail.errorMessage.setValue(nil)
        }){ e in }
        vmDetail.pokemon.collect(collector: Collector<Pokemon> { pokemon in
            print("\ndebug-pokemon: \(String(describing: pokemon?.id)) \(String(describing: pokemon?.name))\n")
            guard let p = pokemon else { return }
            guard let n = p.name else { return }
            if n == self.selected {
                self.onSelected(p)
            }
            self.vmDetail.pokemon.setValue(nil)
        }){ e in }
        vmDetail.nextPokemon.collect(collector: Collector<Pokemon> { pokemon in
            print("\ndebug-nextPokemon: \(String(describing: pokemon?.id)) \(String(describing: pokemon?.name))\n")
            guard let p = pokemon else { return }
            guard let n = p.name else { return }
            if n == self.nextPokemonName {
                self.detailContainerView.setEvolution(item: p)
            }
            self.vmDetail.nextPokemon.setValue(nil)
        }){ e in }
        vmDetail.onUpdateParent.collect(collector: Collector<PokemonUrl> { pokemonUrl in
            print("\ndebug-onUpdateParent: \(String(describing: pokemonUrl?.order))\n")
            guard let p = pokemonUrl else { return }
            if p.isChosen == true {
                guard let n = p.name else { return }
                self.onChosen(n)
            } else {
                self.notifyItemAdapter(p)
            }
            self.vmDetail.onUpdateParent.setValue(nil)
        }){ e in }
    }

    private func onLoading(_ isLoading: Bool?) {
        if let showLoading = isLoading {
            if pokemonAdapter.itemsCache.count == 0 && showLoading {
                pokemonAdapter.showSkeletons()
            } else {
                refreshControl.endRefreshing()
            }
        }
    }
    
    private func onSelected(_ pokemon: Pokemon) {
        view.endEditing(true)
        vmDetail.id = Int32(truncating: pokemon.id!)
        var next: String? = nil
        if let e = pokemon.evolution {
            next = vmDetail.checkEvolution(pokemonEvolution: e, name: pokemon.name!)
        }
        nextPokemonName = next
        detailContainerView.initSubviews()
        detailContainerView.configure(
            item: pokemon,
            next: next,
            onPrimaryAction: {
                self.selected = ""
                self.vmDetail.add(nameArg: pokemon.name!)
                self.navigationController?.navigationBar.isHidden = false
            },
            onSecondaryAction: {
                self.selected = ""
                self.detailContainerView.isHidden = true
                self.navigationController?.navigationBar.isHidden = false
                self.vm.getPokemonsLocal(limit: 1, searchArg: pokemon.name!)
            }
        )
        navigationController?.navigationBar.isHidden = true
        detailContainerView.isHidden = false
    }
    
    private func onChosen(_ name: String) {
        Sound.play(file: "fantasy", fileExtension: "mp3", numberOfLoops: 0)
        let isMultiple = Preferences.value(forKey: AppConstant.shared.MULTIPLE_KEY, defaultValue: false)
        if isMultiple {
            detailContainerView.isHidden = true
            vm.getPokemonsLocal(limit: 1, searchArg: name)
        } else {
            Preferences.value(forKey: AppConstant.shared.EVOLVING_KEY, value: name)
            dismiss(animated: true)
        }
    }
    
    private func notifyAdapter(_ pokemonUrls: [PokemonUrl]) {
        pokemonAdapter.addItems(pokemonUrls.count) {
            for it in pokemonUrls {
                self.pokemonAdapter.items.append(it)
                self.pokemonAdapter.itemsCache.append(it)
            }
        }
        emptyInfoView.isHidden = pokemonAdapter.items.count != 0
        for it in pokemonUrls {
            if it.pokemon == nil || it.pokemon?.image == nil {
                self.vmDetail.getPokemonDetailLocal(nameArg: it.name!)
            }
        }
    }
    
    private func notifyItemAdapter(_ pokemonUrl: PokemonUrl?) {
        guard let it = pokemonUrl else { return }
        print("\ndebug-notifyItemAdapter: \(String(describing: it.order))\n")
        if let o = it.order, Int(truncating: o) < pokemonAdapter.items.count {
            print("\ndebug-notifyItemAdapter-filtered: \(o)\n")
            self.pokemonAdapter.items[Int(truncating: o)] = it
            self.pokemonAdapter.itemsCache[Int(truncating: o)] = it
            self.pokemonAdapter.reloadItems(at: [IndexPath(row: Int(truncating: o), section: 0)])
        }
    }
}

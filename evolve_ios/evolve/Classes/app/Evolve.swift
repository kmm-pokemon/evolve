import evolve_shared
import Toaster
import UIKit

public class Collector<T>: Kotlinx_coroutines_coreFlowCollector {
    let callback:(T?) -> Void

    public init(callback: @escaping (T?) -> Void) {
        self.callback = callback
    }
    
    public func emit(value: Any?, completionHandler: @escaping (Error?) -> Void) {
        callback(value as? T)
        completionHandler(nil)
    }
}

extension Toast {
    static func showTop(message: String, textColor: UIColor, backgroundColor: UIColor) {
        let marginTop: CGFloat = 80.0
        ToastView.appearance().bottomOffsetPortrait = max(UIScreen.main.bounds.width, UIScreen.main.bounds.height) - marginTop
        ToastView.appearance().bottomOffsetLandscape = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height) - marginTop
        ToastView.appearance().textColor = textColor
        ToastView.appearance().backgroundColor = backgroundColor
        Toast(text: message).show()
    }
    static func showSuccess(_ message: String) {
        Toast.showTop(message: message, textColor: .white, backgroundColor: .systemGreen)
    }
    static func showError(_ message: String) {
        Toast.showTop(message: message, textColor: .white, backgroundColor: .systemRed)
    }
}

extension UIScrollView {
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
   }
}

extension UIView {
    public func showShimmerFromBottom(_ show: Bool) {
        if !show {
            layer.mask = .none
        } else {
            let gradient = CAGradientLayer()
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: -0.02, y: 1)
            gradient.frame = CGRect(x: 0, y: 0, width: bounds.size.width, height: bounds.size.height * 3)

            let solid = UIColor(white: 1, alpha: 1).cgColor
            let clear = UIColor(white: 1, alpha: 0.5).cgColor
            gradient.colors     = [ solid, solid, clear, clear, solid, solid ]
            gradient.locations  = [ 0,     0.3,   0.45,  0.55,  0.7,   1     ]

            let theAnimation = CABasicAnimation(keyPath: "transform.translation.y")
            theAnimation.duration = 1.5
            theAnimation.repeatCount = Float.infinity
            theAnimation.autoreverses = false
            theAnimation.isRemovedOnCompletion = false
            theAnimation.fillMode = CAMediaTimingFillMode.forwards
            theAnimation.fromValue = 0
            theAnimation.toValue = -frame.size.height * 1.5
            gradient.add(theAnimation, forKey: "animateLayer")
            
            layer.mask = gradient
        }
    }
}

import UIKit
import core
import core_shared
import evolve_shared
import SwiftySound
import Toaster

class DetailNavController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var detailContainerView: PokemonDetailView!
    
    private let vm = PokemonDetailViewModel()
    private var initialized = false
    var nameArg: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = .primary
        detailContainerView.initialWithBerry = true
        detailContainerView.onDroppedBerry = { berryName in
            if let berry = self.detailContainerView.berryAdapter.items.first(where: { $0.name == berryName }) {
                self.vm.feed(givenBerry: berry)
            }
        }
        detailContainerView.initSubviews()
        setupObserver()
        
        let evolving = Preferences.value(forKey: AppConstant.shared.EVOLVING_KEY, defaultValue: "")
        vm.name = nameArg ?? evolving
        
        if !initialized {
            vm.getPokemonDetailLocal(nameArg: nil)
            initialized = true
        }
    }
    
    private func vibrate() {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
    }
    
    private func setupObserver() {
        vm.loadingIndicator.collect(collector: Collector<Bool> { isLoading in
            if let l = isLoading { self.view.showFulLoading(l) }
        }){ e in }
        vm.successMessage.collect(collector: Collector<String> { message in
            if let m = message { Toast.showSuccess(m) }
            self.vm.successMessage.setValue(nil)
        }){ e in }
        vm.errorMessage.collect(collector: Collector<String> { message in
            if let m = message { Toast.showError(m) }
            self.vm.errorMessage.setValue(nil)
        }){ e in }
        vm.pokemon.collect(collector: Collector<Pokemon> { pokemon in
            guard let p = pokemon else { return }
            guard let n = p.name else { return }
            self.vm.id = Int32(truncating: p.id!)
            var next: String? = nil
            if let e = p.evolution {
                next = self.vm.checkEvolution(pokemonEvolution: e, name: n)
            }
            self.detailContainerView.configure(
                item: p,
                next: next,
                onPrimaryAction: {
                    self.vm.evolve()
                },
                onSecondaryAction: {
                    let confirmDelete = UIAlertController(title: "Delete this pokemon?", message: "All data related to this pokemon will also be deleted.", preferredStyle: .alert)
                    confirmDelete.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (action: UIAlertAction!) in
                        Preferences.value(forKey: AppConstant.shared.EVOLVING_KEY, value: "")
                        self.vm.delete()
                    }))
                    confirmDelete.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    self.present(confirmDelete, animated: true, completion: nil)
                }
            )
        }){ e in }
        vm.nextPokemon.collect(collector: Collector<Pokemon> { pokemon in
            guard let p = pokemon else { return }
            self.detailContainerView.setEvolution(item: p)
            self.vm.getBerriesLocal()
        }){ e in }
        vm.berries.collect(collector: Collector<[Berry]> { berries in
            guard let bs = berries else { return }
            self.detailContainerView.setBerries(bs)
        }){ e in }
        vm.emotion.collect(collector: Collector<String?> { e in
            switch e {
                case "happy":
                    Sound.play(file: "yipee", fileExtension: "mp3", numberOfLoops: 0)
                    self.vibrate()
                case "sad":
                    Sound.play(file: "sad", fileExtension: "mp3", numberOfLoops: 0)
                    self.vibrate()
                case "evolved":
                    Sound.play(file: "evolution", fileExtension: "mp3", numberOfLoops: 0)
                    self.vibrate()
                case "erased":
                    Sound.play(file: "trash", fileExtension: "mp3", numberOfLoops: 0)
                    self.vibrate()
                default: return
            }
            self.vm.emotion.setValue(nil)
        }){ e in }
        vm.onEvolve.collect(collector: Collector<String> { name in
            if let n = name {
                Preferences.value(forKey: AppConstant.shared.EVOLVING_KEY, value: n)
            }
            self.vm.onEvolve.setValue(nil)
        }){ e in }
        vm.onUpdateParent.collect(collector: Collector<PokemonUrl> { pokemonUrl in
            if let _ = pokemonUrl {
                let isMultiple = Preferences.value(forKey: AppConstant.shared.MULTIPLE_KEY, defaultValue: false)
                if !isMultiple {
                    self.dismiss(animated: true)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            self.vm.onUpdateParent.setValue(nil)
        }){ e in }
    }
}

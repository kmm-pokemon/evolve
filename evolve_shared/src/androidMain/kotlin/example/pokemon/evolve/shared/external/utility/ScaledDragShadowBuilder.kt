package example.pokemon.evolve.shared.external.utility

import android.graphics.Canvas
import android.graphics.Point
import android.view.View

class ScaledDragShadowBuilder(view: View, private val scale: Float) : View.DragShadowBuilder(view) {

    override fun onProvideShadowMetrics(outShadowSize: Point?, outShadowTouchPoint: Point?) {
        val scaledWidth = (view.width * scale).toInt()
        val scaledHeight = (view.height * scale).toInt()

        outShadowSize?.set(scaledWidth, scaledHeight)

        outShadowTouchPoint?.set(scaledWidth / 2, scaledHeight / 2)
    }

    override fun onDrawShadow(canvas: Canvas?) {
        canvas?.scale(scale, scale)
        view.draw(canvas)
    }
}
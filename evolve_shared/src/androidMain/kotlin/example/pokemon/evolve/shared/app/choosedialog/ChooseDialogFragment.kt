package example.pokemon.evolve.shared.app.choosedialog

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import example.pokemon.core.shared.app.common.BaseDialogFragment
import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.evolve.shared.R
import example.pokemon.evolve.shared.databinding.ChooseDialogFragmentBinding
import example.pokemon.evolve.shared.external.constant.EvolveConstant

class ChooseDialogFragment :
    BaseDialogFragment<ChooseDialogFragmentBinding>(R.layout.choose_dialog_fragment) {
    override fun isCancelable() = false
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.mbChooseDialogMultiple.setOnClickListener {
            sharedPreferences.edit().remove(AppConstant.EVOLVING_KEY)
                .putBoolean(AppConstant.MULTIPLE_KEY, true).apply()
            setFragmentResult(EvolveConstant.TYPE_KEY, bundleOf(EvolveConstant.TYPE_KEY to true))
            findNavController().navigateUp()
        }
        binding.mbChooseDialogSingle.setOnClickListener {
            sharedPreferences.edit().remove(AppConstant.MULTIPLE_KEY).apply()
            setFragmentResult(EvolveConstant.TYPE_KEY, bundleOf(EvolveConstant.TYPE_KEY to true))
            findNavController().navigateUp()
        }
    }
}
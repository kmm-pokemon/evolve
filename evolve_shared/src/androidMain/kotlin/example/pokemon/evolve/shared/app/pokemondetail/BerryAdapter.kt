package example.pokemon.evolve.shared.app.pokemondetail

import android.content.ClipData
import android.content.ClipDescription
import android.graphics.Color
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.internal.ViewUtils
import example.pokemon.core.shared.R
import example.pokemon.core.shared.external.extension.loadImage
import example.pokemon.evolve.shared.databinding.BerryItemBinding
import example.pokemon.evolve.shared.domain.evolve.entity.Berry
import example.pokemon.evolve.shared.external.utility.ScaledDragShadowBuilder

class BerryAdapter(
    private val widthRatio: Double = 1.0,
    private val height: Int = 0,
) : RecyclerView.Adapter<BerryAdapter.ViewHolder>() {
    var items = mutableListOf<Berry>()
    var types = mutableListOf<String>()

    inner class ViewHolder(
        private val binding: BerryItemBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        private fun isLoading(item: Berry) = item.id == 0

        @Suppress("kotlin:S1186")
        fun bind(position: Int) {
            val item = items[position]
            val context = binding.root.context
            if (isLoading(item)) {
                val anim = AnimationUtils.loadAnimation(context, R.anim.left_right)
                binding.shine.isVisible = true
                binding.shine.startAnimation(anim)
                anim.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(p0: Animation?) {
                    }

                    override fun onAnimationEnd(p0: Animation?) {
                        binding.shine.startAnimation(anim)
                    }

                    override fun onAnimationRepeat(p0: Animation?) {
                    }

                })
            } else {
                binding.shine.isVisible = false
                item.image?.let {
                    binding.ivBerryItem.apply {
                        tag = position.toString()
                        loadImage(it)
                        setBackgroundResource(if (item.naturalGiftType?.name in types) R.drawable.bg_black50_round8 else android.R.color.transparent)
                        setImageOnTouchListener(this)
                        setImageOnDragListener(this)
                    }
                }
            }
        }

        @Suppress("ClickableViewAccessibility")
        private fun setImageOnTouchListener(imageView: ImageView) {
            imageView.setOnTouchListener { v, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        val dragClipData = ClipData(
                            imageView.tag as? CharSequence,
                            arrayOf(ClipDescription.MIMETYPE_TEXT_PLAIN),
                            ClipData.Item(imageView.tag as? CharSequence)
                        )
                        v.startDragAndDrop(
                            dragClipData,
                            ScaledDragShadowBuilder(imageView, 2.5f),
                            null,
                            0,
                        )
                        true
                    }
                    MotionEvent.ACTION_UP -> {
                        v.performClick()
                        false
                    }
                    else -> false
                }
            }
        }

        private fun setImageOnDragListener(imageView: ImageView) {
            imageView.setOnDragListener { v, event ->
                when (event.action) {
                    DragEvent.ACTION_DRAG_STARTED -> {
                        if (event.clipDescription.label == imageView.tag) {
                            (v as? ImageView)?.setColorFilter(Color.LTGRAY)
                            v.invalidate()
                            true
                        } else false
                    }
                    DragEvent.ACTION_DRAG_ENDED -> {
                        (v as? ImageView)?.clearColorFilter()
                        v.invalidate()
                        true
                    }
                    else -> false
                }
            }
        }
    }

    @Suppress("RestrictedApi")
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding =
            BerryItemBinding.inflate(
                LayoutInflater.from(viewGroup.context),
                viewGroup,
                false
            )
        if (widthRatio != 1.0) binding.root.layoutParams.width =
            (viewGroup.measuredWidth * widthRatio).toInt()
        if (height != 0) binding.root.layoutParams.height =
            ViewUtils.dpToPx(binding.root.context, height).toInt()
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(position)
    }

    override fun getItemCount() = items.size

    fun clear() {
        val size = items.size
        items = mutableListOf()
        notifyItemRangeRemoved(0, size)
    }

    fun addItem(process: () -> Unit) {
        if (items.size > 0) clear()
        val sizeBefore = items.size
        process.invoke()
        val sizeAfter = items.size
        notifyItemRangeInserted(sizeBefore, sizeAfter - 1)
    }
}
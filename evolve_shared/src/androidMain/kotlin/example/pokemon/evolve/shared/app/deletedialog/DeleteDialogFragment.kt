package example.pokemon.evolve.shared.app.deletedialog

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import example.pokemon.core.shared.app.common.BaseDialogFragment
import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.evolve.shared.R
import example.pokemon.evolve.shared.databinding.DeleteDialogFragmentBinding

class DeleteDialogFragment :
    BaseDialogFragment<DeleteDialogFragmentBinding>(R.layout.delete_dialog_fragment) {
    override fun isCancelable() = false
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.mbDeleteDialog.setOnClickListener {
            sharedPreferences.edit().remove(AppConstant.EVOLVING_KEY).apply()
            setFragmentResult(AppConstant.DELETED_KEY, bundleOf(AppConstant.DELETED_KEY to true))
            findNavController().navigateUp()
        }
        binding.mbDeleteDialogCancel.setOnClickListener {
            findNavController().navigateUp()
        }
    }
}
package example.pokemon.evolve.shared.app.collectionlist

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import example.pokemon.core.shared.app.common.BaseFragment
import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.core.shared.external.extension.*
import example.pokemon.evolve.shared.R
import example.pokemon.evolve.shared.app.common.PokemonAdapter
import example.pokemon.evolve.shared.app.common.PokemonDetailViewModel
import example.pokemon.evolve.shared.app.common.PokemonListViewModel
import example.pokemon.evolve.shared.databinding.CollectionsListFragmentBinding
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import example.pokemon.evolve.shared.external.utility.GridSpacingItemDecoration
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import example.pokemon.core.shared.R as cR

class CollectionListFragment :
    BaseFragment<CollectionsListFragmentBinding>(R.layout.collections_list_fragment) {
    private val vm: PokemonListViewModel by viewModel()
    private val vmDetail: PokemonDetailViewModel by viewModel()

    private lateinit var pokemonAdapter: PokemonAdapter
    private lateinit var autoComplete: ArrayAdapter<String>

    override fun showBottomNavBar() = sharedPreferences.getBoolean(AppConstant.MULTIPLE_KEY, false)
    override fun actionBarTitle() = getString(cR.string.menu_collections)
    override fun expandActionBar() = true
    override fun actionBarHeight() = 125

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupHeader()
        setupObserver()

        binding.srlCollections.setOnRefreshListener {
            load(0, "pull to refresh")
        }
        pokemonAdapter = PokemonAdapter(1.0, 200, 8, 2).apply {
            enableFilter = false
            skeleton = PokemonUrl().apply { name = "" }
            onSelected = { _, i ->
                goTo(getString(R.string.route_pokemon_detail).replace("{name}", i.name.toString()))
            }
            fetchData = {
                vm.page++
                load(vm.page, "reach bottom")
            }
        }
        binding.rvPokemonCollection.apply {
            adapter = pokemonAdapter
            addOnScrollListener(pokemonAdapter.Listener(binding.srlCollections))
            if (itemDecorationCount > 0) removeItemDecorationAt(0)
            addItemDecoration(GridSpacingItemDecoration(2, 10, false))
        }

        load(0, "initial", 100000)
    }

    private fun setupHeader() {
        actionBarExpandedDescription()?.isVisible = false
        actionBarCollapsingLayout()?.expandedTitleMarginBottom = dpToPx(70f)
        if (showBottomNavBar()) binding.clCollections.setPadding(0, 0, 0, dpToPx(60f))

        autoComplete = ArrayAdapter(requireContext(), android.R.layout.simple_dropdown_item_1line)
        actionBarExpandedAutoComplete()?.apply {
            text.clear()
            vm.search = ""
            clearFocus()
            isVisible = true
            setAdapter(autoComplete)
            setOnItemClickListener { _, _, position, _ ->
                vm.search = autoComplete.getItem(position) ?: ""
                load(0, "on select auto complete")
            }
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    vm.search = text.toString()
                    load(0, "enter search keyboard")
                    return@setOnEditorActionListener true
                }
                false
            }
        }
    }

    private fun setupObserver() {
        binding.lifecycleOwner = viewLifecycleOwner
        binding.vm = vm.also {
            it.isChosen = true
            it.loadingIndicator.launchAndCollectIn(this, Lifecycle.State.STARTED) { l ->
                onLoading(l)
            }
            it.successMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showSuccessSnackbar(m)
                it.successMessage.value = null
            }
            it.errorMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showErrorSnackbar(m)
                it.errorMessage.value = null
            }
            it.toastMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showToast(m)
                it.toastMessage.value = null
            }
            it.allPokemons.launchAndCollectIn(this, Lifecycle.State.STARTED) { aps ->
                aps?.let { its ->
                    val temp = mutableListOf<String>()
                    for (item in its) item.name?.takeIf { n -> n.isNotEmpty() }
                        ?.let { n -> temp.add(n) }
                    if (temp.isNotEmpty()) {
                        autoComplete.clear()
                        autoComplete.addAll(temp)
                        load(0, "after get all pokemon")
                    }
                }
                it.allPokemons.value = null
            }
            it.pokemons.launchAndCollectIn(this, Lifecycle.State.STARTED) { ps ->
                notifyAdapter(ps)
                it.pokemons.value = null
            }
        }

        binding.vmDetail = vmDetail.also {
            it.log.launchAndCollectIn(this, Lifecycle.State.STARTED) { l ->
                Timber.d("vmDetail-log: $l")
                it.log.value = null
            }
        }
    }

    private fun onLoading(isLoading: Boolean?) {
        if (pokemonAdapter.itemsCache.size == 0 && isLoading == true) {
            binding.rvPokemonCollection.post { pokemonAdapter.showSkeletons() }
        }
    }

    private fun load(p: Int, f: String, limit: Int = if (vm.search.isEmpty()) AppConstant.LIST_LIMIT else 1) {
        Timber.d("load: $p from $f")
        hideKeyboard()
        vm.page = p
        if (p == 0) binding.rvPokemonCollection.post { pokemonAdapter.clear() }
        vm.getPokemonsLocal(limit)
    }

    private fun notifyAdapter(pokemons: MutableList<PokemonUrl>?) {
        pokemons?.let { its ->
            binding.rvPokemonCollection.post {
                pokemonAdapter.addItem {
                    pokemonAdapter.items.addAll(its)
                    pokemonAdapter.itemsCache.addAll(its)
                }
                binding.tvEmpty.isVisible = pokemonAdapter.items.size == 0
            }
        }
        binding.srlCollections.isRefreshing = false
    }
}
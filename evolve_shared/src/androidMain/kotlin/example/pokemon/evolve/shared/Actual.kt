package example.pokemon.evolve.shared

import example.pokemon.evolve.shared.app.common.PokemonDetailViewModel
import example.pokemon.evolve.shared.app.common.PokemonListViewModel
import example.pokemon.evolve.shared.app.onboarding.OnBoardingViewModel
import example.pokemon.evolve.shared.data.evolve.EvolveRepositoryImpl
import example.pokemon.evolve.shared.domain.evolve.EvolveRepository
import example.pokemon.evolve.shared.domain.evolve.usecase.*
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

actual fun libModule() = module {
    singleOf(::provideEvolveDb)

    single<EvolveRepository> { EvolveRepositoryImpl() }

    singleOf(::GetPokemonsNetworkUseCase)
    singleOf(::GetPokemonsLocalUseCase)
    singleOf(::SetPokemonsLocalUseCase)
    singleOf(::SetPokemonLocalUseCase)

    singleOf(::GetPokemonDetailNetworkUseCase)
    singleOf(::GetPokemonDetailLocalUseCase)
    singleOf(::SetPokemonDetailLocalUseCase)

    singleOf(::GetBerriesNetworkUseCase)
    singleOf(::GetBerriesLocalUseCase)
    singleOf(::FeedPokemonUseCase)

    viewModel { OnBoardingViewModel() }
    viewModel { PokemonListViewModel() }
    viewModel { PokemonDetailViewModel() }
}
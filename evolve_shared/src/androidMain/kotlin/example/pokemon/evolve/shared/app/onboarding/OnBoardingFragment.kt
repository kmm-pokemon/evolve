package example.pokemon.evolve.shared.app.onboarding

import android.os.Bundle
import android.view.View
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import com.google.android.material.tabs.TabLayoutMediator
import example.pokemon.core.shared.app.common.BaseActivity
import example.pokemon.core.shared.app.common.BaseFragment
import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.core.shared.external.extension.goTo
import example.pokemon.core.shared.external.extension.launchAndCollectIn
import example.pokemon.core.shared.external.extension.showErrorSnackbar
import example.pokemon.core.shared.external.extension.showSuccessSnackbar
import example.pokemon.evolve.shared.R
import example.pokemon.evolve.shared.databinding.OnBoardingFragmentBinding
import example.pokemon.evolve.shared.external.constant.EvolveConstant
import org.koin.androidx.viewmodel.ext.android.viewModel

class OnBoardingFragment : BaseFragment<OnBoardingFragmentBinding>(R.layout.on_boarding_fragment) {
    private val vm: OnBoardingViewModel by viewModel()

    override fun isFullScreen() = true
    override fun showActionBar() = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(EvolveConstant.TYPE_KEY) { _, b ->
            if (b.getBoolean(EvolveConstant.TYPE_KEY)) {
                sharedPreferences.edit().putBoolean(AppConstant.ONBOARDING_KEY, true).apply()
                vm.choose()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.vm = vm.also {
            it.loadingIndicator.launchAndCollectIn(this, Lifecycle.State.STARTED) { l ->
                showFullLoading(l)
            }
            it.successMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showSuccessSnackbar(m)
                it.errorMessage.value = null
            }
            it.errorMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showErrorSnackbar(m)
                it.errorMessage.value = null
            }
            it.onNext.launchAndCollectIn(this, Lifecycle.State.STARTED) { ok ->
                onNext(ok)
                it.onNext.value = false
            }
            it.hasChosen.launchAndCollectIn(this, Lifecycle.State.STARTED) { ok ->
                if (ok) {
                    (activity as BaseActivity).navGraph()?.let { g ->
                        findNavController().graph.clear()
                        findNavController().setGraph(g)
                    }
                }
                it.hasChosen.value = false
            }
        }

        val images = listOf(R.drawable.explore_world, R.drawable.start_evolve)
        val adapter = OnBoardingAdapter(images)
        binding.vpOnboarding.adapter = adapter
        binding.mbSkip.setOnClickListener { showChooseDialog() }
        TabLayoutMediator(binding.tlOnboarding, binding.vpOnboarding) { _, _ -> }.attach()
    }

    private fun onNext(next: Boolean) {
        if (next) {
            when (binding.vpOnboarding.currentItem) {
                0 -> binding.vpOnboarding.currentItem = 1
                else -> showChooseDialog()
            }
        }
    }

    private fun showChooseDialog() {
        goTo(getString(R.string.route_choose_dialog))
    }
}
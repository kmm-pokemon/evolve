package example.pokemon.evolve.shared.app.onboarding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import example.pokemon.evolve.shared.R
import example.pokemon.evolve.shared.databinding.OnBoardingItemBinding

class OnBoardingAdapter(
    private val items: List<Int>,
) : RecyclerView.Adapter<OnBoardingAdapter.ViewHolder>() {

    class ViewHolder(private val binding: OnBoardingItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Int) {
            val context = binding.root.context
            binding.ivOnboardingItem.setImageResource(item)
            when (item) {
                R.drawable.explore_world -> {
                    binding.tvOnboardingItemTitle.text =
                        context.getString(R.string.onboarding_1_title)
                    binding.tvOnboardingItemSubtitle.text =
                        context.getString(R.string.onboarding_1_subtitle)
                }
                R.drawable.start_evolve -> {
                    binding.tvOnboardingItemTitle.text =
                        context.getString(R.string.onboarding_2_title)
                    binding.tvOnboardingItemSubtitle.text =
                        context.getString(R.string.onboarding_2_subtitle)
                }
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            OnBoardingItemBinding.inflate(
                LayoutInflater.from(viewGroup.context),
                viewGroup,
                false
            )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(items[position])
    }

    override fun getItemCount() = items.size

}
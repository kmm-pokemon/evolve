package example.pokemon.evolve.shared.app.common

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.android.material.internal.ViewUtils
import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.evolve.shared.R
import example.pokemon.evolve.shared.databinding.PokemonItemBinding
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import example.pokemon.core.shared.R as cR


class PokemonAdapter(
    private val widthRatio: Double = 1.0,
    private val height: Int = 0,
    private val radius: Int = 0,
    private val elevation: Int = 0,
) : RecyclerView.Adapter<PokemonAdapter.ViewHolder>() {

    var items = mutableListOf<PokemonUrl>()
    var itemsCache = mutableListOf<PokemonUrl>()
    var isLastPage = false
    var skeleton = PokemonUrl()
    var fetchData: (() -> Unit)? = null
    var onSelected: ((View, PokemonUrl) -> Unit)? = null
    var enableFilter = true

    @Suppress("RestrictedApi")
    inner class ViewHolder(
        private val binding: PokemonItemBinding,
    ) : RecyclerView.ViewHolder(binding.root) {

        private fun isLoading(item: PokemonUrl) = item.name == ""

        @Suppress("kotlin:S1186", "SetTextI18n")
        fun bind(item: PokemonUrl) {
            val context = binding.root.context
            if (isLoading(item)) {
                val anim = AnimationUtils.loadAnimation(context, cR.anim.left_right)
                binding.shine.isVisible = true
                binding.shine.startAnimation(anim)
                anim.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(p0: Animation?) {
                    }

                    override fun onAnimationEnd(p0: Animation?) {
                        binding.shine.startAnimation(anim)
                    }

                    override fun onAnimationRepeat(p0: Animation?) {
                    }

                })
            } else binding.shine.isVisible = false

            binding.cvPokemonItem.radius = ViewUtils.dpToPx(context, radius)
            binding.cvPokemonItem.elevation = ViewUtils.dpToPx(context, elevation)
            if (elevation > 0) binding.cvPokemonItem.useCompatPadding = true

            if (!isLoading(item)) {
                if (item.isChosen != true || !enableFilter) binding.root.setOnClickListener {
                    onSelected?.invoke(binding.ivPokemonItem, item)
                }
                binding.tvPokemonItemNumber.apply {
                    isVisible = true
                    text = "#${item.order!! + 1}"
                }
                val progress = CircularProgressDrawable(context)
                progress.strokeWidth = 5f
                progress.centerRadius = 30f
                progress.start()
                binding.ivPokemonItem.apply {
                    item.pokemon?.image?.let {
                        Glide.with(context)
                            .setDefaultRequestOptions(
                                RequestOptions().placeholder(progress).error(R.drawable.image_404)
                            )
                            .load(item.pokemon?.image)
                            .listener(object : RequestListener<Drawable> {
                                override fun onLoadFailed(
                                    e: GlideException?,
                                    model: Any?,
                                    target: Target<Drawable>?,
                                    isFirstResource: Boolean
                                ): Boolean {
                                    progress.stop()
                                    return false
                                }

                                override fun onResourceReady(
                                    resource: Drawable?,
                                    model: Any?,
                                    target: Target<Drawable>?,
                                    dataSource: com.bumptech.glide.load.DataSource?,
                                    isFirstResource: Boolean
                                ): Boolean {
                                    progress.stop()
                                    if (item.isChosen == true && enableFilter) setColorFilter(Color.LTGRAY)
                                    else clearColorFilter()
                                    return false
                                }
                            })
                            .into(this)
                    } ?: run {
                        setImageDrawable(progress)
                    }
                }
            } else binding.tvPokemonItemNumber.isVisible = false
        }
    }

    inner class Listener(private val swipeRefreshLayout: SwipeRefreshLayout) :
        RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            val topRowVerticalPosition =
                if (recyclerView.childCount == 0) 0 else recyclerView.getChildAt(
                    0
                ).top
            swipeRefreshLayout.isEnabled = topRowVerticalPosition >= 0
            if (!recyclerView.canScrollVertically(1) && !isLastPage) {
                fetchData?.invoke()
            }
        }
    }

    @Suppress("RestrictedApi")
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            PokemonItemBinding.inflate(
                LayoutInflater.from(viewGroup.context),
                viewGroup,
                false
            )
        if (widthRatio != 1.0) binding.root.layoutParams.width =
            (viewGroup.measuredWidth * widthRatio).toInt()
        if (height != 0) binding.root.layoutParams.height =
            ViewUtils.dpToPx(binding.root.context, height).toInt()
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(items[position])
    }

    override fun getItemCount() = items.size

    fun clear() {
        val size = items.size
        items = mutableListOf()
        itemsCache = mutableListOf()
        isLastPage = false
        notifyItemRangeRemoved(0, size)
    }

    fun showSkeletons(size: Int = AppConstant.LIST_LIMIT) {
        clear()
        val skeletons = mutableListOf<PokemonUrl>()
        for (i in 0 until size) skeletons.add(skeleton)
        items = skeletons
        notifyItemRangeInserted(0, skeletons.size)
    }

    fun addItem(process: () -> Unit) {
        if (itemsCache.size == 0 && items.size > 0) {
            clear()
        }
        val sizeBefore = items.size
        process.invoke()
        val sizeAfter = items.size
        if ((sizeAfter - sizeBefore) < AppConstant.LIST_LIMIT) {
            isLastPage = true
        }
        notifyItemRangeInserted(sizeBefore, sizeAfter)
    }
}
package example.pokemon.evolve.shared.domain.evolve.entity

import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.Serializable

@Serializable
open class PokemonUrl : RealmObject {
    var order: Int? = null
    var name: String? = null
    var url: String? = null
    var isChosen: Boolean? = null
    var pokemon: Pokemon? = null

    fun clone(): PokemonUrl {
        val pokemonUrl = PokemonUrl()
        pokemonUrl.order = order
        pokemonUrl.name = name
        pokemonUrl.url = url
        pokemonUrl.isChosen = isChosen
        pokemonUrl.pokemon = pokemon?.clone()
        return pokemonUrl
    }
}
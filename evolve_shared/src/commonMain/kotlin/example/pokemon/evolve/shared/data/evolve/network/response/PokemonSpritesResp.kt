package example.pokemon.evolve.shared.data.evolve.network.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PokemonSpritesResp(
    var other: PokemonSpritesOther? = null,
)

@Serializable
data class PokemonSpritesOther(
    @SerialName("official-artwork")
    var officialArtwork: PokemonSpritesItem? = null
)

@Serializable
data class PokemonSpritesItem(
    var default: String? = null,
    @SerialName("front_default")
    var frontDefault: String? = null
)
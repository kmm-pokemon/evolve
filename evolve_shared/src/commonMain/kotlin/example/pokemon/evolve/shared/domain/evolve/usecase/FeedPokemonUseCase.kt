package example.pokemon.evolve.shared.domain.evolve.usecase

import example.pokemon.evolve.shared.domain.evolve.entity.Berry
import example.pokemon.evolve.shared.domain.evolve.entity.Pokemon

class FeedPokemonUseCase {
    operator fun invoke(
        pokemon: Pokemon?,
        nextPokemon: Pokemon?,
        lastEatenBerry: Berry?,
        givenBerry: Berry?
    ): Int {
        if (pokemon?.weight == null) {
            throw Exception("Pokemon weight data not found.")
        }
        if (nextPokemon?.weight == null) {
            throw Exception("Next pokemon weight data not found.")
        }
        if (givenBerry?.weight == null) {
            throw Exception("Given berry weight data not found.")
        }
        if (givenBerry.firmnessName == null) {
            throw Exception("Given berry firmness data not found.")
        }
        if (lastEatenBerry?.firmnessName == givenBerry.firmnessName) {
            val decrease = pokemon.weight!! - givenBerry.weight!! * 2
            if (decrease <= 0) {
                throw Exception(
                    "Please, don't give me ${
                        lastEatenBerry?.firmnessName?.replace(
                            "-",
                            " "
                        )
                    } berry again!"
                )
            }
            return decrease
        }

        return pokemon.weight!! + givenBerry.weight!!
    }
}
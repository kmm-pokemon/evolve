package example.pokemon.evolve.shared.app.common

import example.pokemon.core.shared.app.common.BaseViewModel
import example.pokemon.evolve.shared.domain.evolve.entity.Berry
import example.pokemon.evolve.shared.domain.evolve.entity.Pokemon
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonEvolution
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import example.pokemon.evolve.shared.domain.evolve.usecase.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class PokemonDetailViewModel : KoinComponent, BaseViewModel() {
    private val getPokemonsLocalUseCase: GetPokemonsLocalUseCase by inject()
    private val setPokemonLocalUseCase: SetPokemonLocalUseCase by inject()

    private val getPokemonDetailNetworkUseCase: GetPokemonDetailNetworkUseCase by inject()
    private val getPokemonDetailLocalUseCase: GetPokemonDetailLocalUseCase by inject()
    private val setPokemonDetailLocalUseCase: SetPokemonDetailLocalUseCase by inject()

    private val getBerriesNetworkUseCase: GetBerriesNetworkUseCase by inject()
    private val getBerriesLocalUseCase: GetBerriesLocalUseCase by inject()
    private val feedPokemonUseCase: FeedPokemonUseCase by inject()

    var id = 0
    var name = ""
    var lastEatenBerry: Berry? = null
    var pokemon = MutableStateFlow<Pokemon?>(null)
    var nextPokemon = MutableStateFlow<Pokemon?>(null)
    var berries = MutableStateFlow<MutableList<Berry>?>(null)
    var onUpdateParent = MutableStateFlow<PokemonUrl?>(null)
    var onEvolve = MutableStateFlow<String?>(null)
    var log = MutableStateFlow<String?>(null)
    var emotion = MutableStateFlow<String?>(null)
    var useAsyncNetworkCall = true

    fun getPokemonDetailLocal(nameArg: String? = null) {
        scope.launch {
            loadingIndicator.value = true
            val pokemonLocal = getPokemonDetailLocalUseCase(nameArg ?: name)
            pokemonLocal?.let {
                if (name.isEmpty() || name == it.name) pokemon.value = it
                else nextPokemon.value = it
                loadingIndicator.value = false
            } ?: run {
                getPokemonDetailNetwork(nameArg)
            }
        }
    }

    fun getPokemonDetailNetwork(nameArg: String? = null) {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            scope.launch { loadingIndicator.value = true }
            try {
                val response = getPokemonDetailNetworkUseCase(nameArg ?: name)
                response?.let {
                    updateParent(coroutine, it.name, pokemonArg = it)
                    scope.launch {
                        if (name.isEmpty() || name == it.name) pokemon.value = it
                        else nextPokemon.value = it
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                scope.launch { pokemon.value = null }
            }
        }
    }

    fun getBerriesLocal() {
        scope.launch {
            val berriessLocal = getBerriesLocalUseCase()
            berriessLocal.takeIf { it.isNotEmpty() }?.let {
                berries.value = it.toMutableList()
                loadingIndicator.value = false
            } ?: run {
                getBerriesNetwork()
            }
        }
    }

    private fun getBerriesNetwork() {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            scope.launch {
                loadingIndicator.value = true
                toastMessage.value = "Downloading berry data..."
            }
            try {
                val response = getBerriesNetworkUseCase(0, 100000)
                scope.launch {
                    berries.value = response
                    loadingIndicator.value = false
                }
            } catch (e: Exception) {
                e.printStackTrace()
                scope.launch { berries.value = mutableListOf() }
            }
        }
    }

    fun feed(givenBerry: Berry) {
        scope.launch {
            loadingIndicator.value = true
            try {
                val newWeight =
                    feedPokemonUseCase(pokemon.value, nextPokemon.value, lastEatenBerry, givenBerry)
                val beforeWeight = pokemon.value?.weight
                pokemon.value?.let {
                    val temp = it.clone().apply { weight = newWeight }
                    if (setPokemonDetailLocalUseCase(temp)) {
                        pokemon.value = temp

                        lastEatenBerry = givenBerry
                        val firmnessName = lastEatenBerry?.firmnessName?.replace("-", " ")
                        if (newWeight > beforeWeight!!) {
                            successMessage.value =
                                "Yummy! I Like this $firmnessName berry! (+${givenBerry.weight})"
                            emotion.value = "happy"
                        } else {
                            errorMessage.value =
                                "Ugh, I already eat $firmnessName berry! (-${givenBerry.weight!! * 2})"
                            emotion.value = "sad"
                        }
                    }
                }
                loadingIndicator.value = false
            } catch (e: Exception) {
                e.printStackTrace()
                loadingIndicator.value = false
                errorMessage.value = e.message
                emotion.value = "sad"
            }
        }
    }

    fun add(nameArg: String? = null) {
        updateParent(scope, nameArg, true)
    }

    fun evolve() {
        scope.launch {
            try {
                pokemon.value?.let {
                    setPokemonDetailLocalUseCase(it.clone().apply { weight = it.weightCache })
                    lastEatenBerry = null
                    updateParent(scope, isChosenArg = false, evolve = true)
                    updateParent(scope, nextPokemon.value?.name, isChosenArg = true)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                loadingIndicator.value = false
                errorMessage.value = e.message
            }
        }
    }

    fun delete() {
        scope.launch {
            try {
                pokemon.value?.let {
                    setPokemonDetailLocalUseCase(it.clone().apply { weight = it.weightCache })
                    updateParent(scope, isChosenArg = false)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                loadingIndicator.value = false
                errorMessage.value = e.message
            }
        }
    }

    private fun updateParent(
        coroutine: CoroutineScope,
        nameArg: String? = null,
        isChosenArg: Boolean? = null,
        evolve: Boolean? = null,
        pokemonArg: Pokemon? = null
    ) {
        coroutine.launch {
            scope.launch { loadingIndicator.value = true }
            try {
                var pokemonUrl = getPokemonsLocalUseCase(0, 1, nameArg ?: name).firstOrNull()
                pokemonUrl?.let { it ->
                    pokemonUrl = it.clone().apply { isChosen = isChosenArg }
                    if (pokemonArg != null) pokemonUrl!!.pokemon = pokemonArg
                    if (setPokemonLocalUseCase(pokemonUrl!!)) {
                        scope.launch { notifySucceedUpdate(isChosenArg, evolve, pokemonUrl!!) }
                    } else {
                        scope.launch { errorMessage.value = "Failed save pokemons!" }
                    }
                } ?: run {
                    scope.launch {
                        errorMessage.value =
                            "Cannot update parent of ${nameArg ?: name}, parent not found!"
                    }
                }
                scope.launch { loadingIndicator.value = false }
            } catch (e: Exception) {
                e.printStackTrace()
                scope.launch {
                    loadingIndicator.value = false
                    errorMessage.value = e.message
                }
            }
        }
    }

    private fun notifySucceedUpdate(
        isChosenArg: Boolean? = null,
        evolve: Boolean? = null,
        pokemonUrl: PokemonUrl
    ) {
        if (evolve == true) {
            successMessage.value =
                "Pokemon has been evolved to ${nextPokemon.value?.name}"
            onEvolve.value = nextPokemon.value?.name
            emotion.value = "evolved"
        } else if (isChosenArg != null) {
            successMessage.value =
                "Pokemon has been ${if (isChosenArg == true) "saved to" else "deleted from"} collections."
            onUpdateParent.value = pokemonUrl
            if (isChosenArg == false) emotion.value = "erased"
        } else onUpdateParent.value = pokemonUrl
    }

    fun checkEvolution(pokemonEvolution: PokemonEvolution?, name: String): String? {
        var nameNextPokemon: String? = null
        pokemonEvolution?.evolvesTo?.takeIf { it.isNotEmpty() }?.get(0)?.let { e ->
            e.species?.let { s ->
                val split = s.url!!.split("/")
                if (name != s.name && id > 0 && split[split.size - 2].toInt() > id) {
                    nameNextPokemon = s.name
                    getPokemonDetailLocal(s.name)
                } else nameNextPokemon = checkEvolution(e, name)
            }
        }
        return nameNextPokemon
    }
}
package example.pokemon.evolve.shared.app.common

import example.pokemon.core.shared.app.common.BaseViewModel
import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import example.pokemon.evolve.shared.domain.evolve.usecase.GetPokemonsLocalUseCase
import example.pokemon.evolve.shared.domain.evolve.usecase.GetPokemonsNetworkUseCase
import example.pokemon.evolve.shared.domain.evolve.usecase.SetPokemonsLocalUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class PokemonListViewModel : KoinComponent, BaseViewModel() {
    private val getPokemonsNetworkUseCase: GetPokemonsNetworkUseCase by inject()
    private val getPokemonsLocalUseCase: GetPokemonsLocalUseCase by inject()
    private val setPokemonsLocalUseCase: SetPokemonsLocalUseCase by inject()

    var page = 0
    var search = ""
    var isChosen: Boolean? = null
    var allPokemons = MutableStateFlow<MutableList<PokemonUrl>?>(null)
    var pokemons = MutableStateFlow<MutableList<PokemonUrl>?>(null)
    var pokemon = MutableStateFlow<PokemonUrl?>(null)
    var useAsyncNetworkCall = true

    fun getPokemonsLocal(limit: Int = AppConstant.LIST_LIMIT, searchArg: String? = null) {
        scope.launch {
            loadingIndicator.value = true
            val pokemonsLocal = getPokemonsLocalUseCase(
                if (searchArg != null) 0 else limit * page,
                limit,
                searchArg ?: search,
                isChosen
            )

            if (searchArg != null) {
                if (pokemonsLocal.isNotEmpty()) pokemon.value = pokemonsLocal[0]
                else pokemon.value = null
            } else {
                if (limit > AppConstant.LIST_LIMIT) {
                    if (pokemonsLocal.isEmpty()) {
                        getPokemonsNetwork()
                        return@launch
                    }
                    allPokemons.value = pokemonsLocal.toMutableList()
                } else pokemons.value = pokemonsLocal.toMutableList()
            }
            loadingIndicator.value = false
        }
    }

    private fun getPokemonsNetwork() {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            try {
                val response = getPokemonsNetworkUseCase(0, 100000)
                if (setPokemonsLocalUseCase(response)) {
                    scope.launch { allPokemons.value = response }
                } else errorMessage.value = "Failed save pokemons."
            } catch (e: Exception) {
                e.printStackTrace()
                scope.launch { pokemons.value = mutableListOf() }
                page = 0
            }
        }
    }
}
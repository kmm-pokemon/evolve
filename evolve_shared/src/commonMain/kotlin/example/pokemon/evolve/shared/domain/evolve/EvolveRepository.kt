package example.pokemon.evolve.shared.domain.evolve

import example.pokemon.evolve.shared.domain.evolve.entity.Berry
import example.pokemon.evolve.shared.domain.evolve.entity.Pokemon
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl

interface EvolveRepository {
    suspend fun getPokemonsNetwork(offset: Int, limit: Int) = mutableListOf<PokemonUrl>()
    suspend fun getPokemonsLocal(
        offset: Int,
        limit: Int,
        search: String,
        isChosen: Boolean?
    ) = listOf<PokemonUrl>()

    suspend fun setPokemonsLocal(pokemons: MutableList<PokemonUrl>) = false
    suspend fun setPokemonLocal(pokemon: PokemonUrl) = false

    suspend fun getPokemonDetailNetwork(name: String): Pokemon? = null
    suspend fun getPokemonDetailLocal(name: String): Pokemon? = null
    suspend fun setPokemonDetailLocal(pokemon: Pokemon) = false

    suspend fun getBerriesNetwork(offset: Int, limit: Int) = mutableListOf<Berry>()
    suspend fun getBerriesLocal() = listOf<Berry>()
}
package example.pokemon.evolve.shared

import example.pokemon.core.shared.Context
import example.pokemon.core.shared.initKoin
import example.pokemon.evolve.shared.domain.evolve.entity.*
import example.pokemon.evolve.shared.domain.evolve.usecase.*
import io.ktor.http.*
import io.ktor.util.reflect.*
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration

fun protocolShared() = URLProtocol.HTTPS
fun prefsNameShared() = "pri0r_m1cr0_h1ght_3v0lv3"
val realmConfig = RealmConfiguration.Builder(
        schema = setOf(
            Berry::class,
            PokemonUrl::class,
            Pokemon::class,
            PokemonStat::class,
            PokemonType::class,
            PokemonEvolution::class,
        )
    )
        .name("3v0Lv3-p0C3.realm")
        .schemaVersion(1)
        .deleteRealmIfMigrationNeeded()
        .build()
var realm = Realm.open(realmConfig)
fun provideEvolveDb() = realm
fun deleteRealm() {
    if (!realm.isClosed()) realm.close()
    Realm.deleteRealm(realmConfig)
    realm = Realm.open(realmConfig)
}

@Suppress("UnUsed")
fun initKoin(
    context: Context?,
    host: String,
    deviceId: String,
    version: String,
) = initKoin(context, host, protocolShared(), prefsNameShared(), deviceId, version) {
    modules(libModule())
}
package example.pokemon.evolve.shared.domain.evolve.entity

import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.Serializable

@Serializable
open class PokemonType : RealmObject {
    var type: PokemonUrl? = null
}
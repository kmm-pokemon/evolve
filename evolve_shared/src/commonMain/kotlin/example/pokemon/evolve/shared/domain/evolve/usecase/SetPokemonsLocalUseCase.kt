package example.pokemon.evolve.shared.domain.evolve.usecase

import example.pokemon.evolve.shared.domain.evolve.EvolveRepository
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SetPokemonsLocalUseCase : KoinComponent {
    private val evolveRepository: EvolveRepository by inject()
    suspend operator fun invoke(pokemons: MutableList<PokemonUrl>) =
        evolveRepository.setPokemonsLocal(pokemons)
}
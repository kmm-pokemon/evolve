package example.pokemon.evolve.shared.domain.evolve.entity

import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
open class PokemonStat : RealmObject {
    @SerialName("base_stat")
    var baseStat: Int? = null
    var stat: PokemonUrl? = null
}
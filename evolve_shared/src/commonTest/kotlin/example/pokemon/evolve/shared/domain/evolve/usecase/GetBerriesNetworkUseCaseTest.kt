package example.pokemon.evolve.shared.domain.evolve.usecase

import example.pokemon.evolve.shared.domain.evolve.EvolveRepository
import example.pokemon.evolve.shared.domain.evolve.entity.Berry
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

@OptIn(ExperimentalCoroutinesApi::class)
class GetBerriesNetworkUseCaseTest : KoinTest {
    private lateinit var uc: GetBerriesNetworkUseCase
    private fun startKoin(evolveRepository: EvolveRepository) {
        stopKoin()
        startKoin {
            modules(module {
                single { evolveRepository }
            })
        }
    }

    /**Should return a list of pokemons when the network call is successful*/
    @Test
    fun invokeWhenNetworkCallIsSuccessfulThenReturnListOfBerries() = runTest {
        val expect = mutableListOf(Berry())
        var getBerriesNetworkCount = 0
        val evolveRepository = object : EvolveRepository {
            override suspend fun getBerriesNetwork(
                offset: Int,
                limit: Int
            ): MutableList<Berry> {
                getBerriesNetworkCount++
                return expect
            }
        }
        startKoin(evolveRepository)

        uc = GetBerriesNetworkUseCase()
        val actual = uc.invoke(0, 10)
        assertEquals(1, getBerriesNetworkCount)
        assertEquals(expect, actual)
        stopKoin()
    }

    /**Should throw an exception when the network call is not successful*/
    @Test
    fun invokeWhenNetworkCallIsNotSuccessfulThenThrowException() = runTest {
        val expect = "Network call is failed"
        val evolveRepository = object : EvolveRepository {
            override suspend fun getBerriesNetwork(
                offset: Int,
                limit: Int
            ): MutableList<Berry> {
                throw Exception(expect)
            }
        }
        startKoin(evolveRepository)

        uc = GetBerriesNetworkUseCase()
        try {
            uc.invoke(0, 10)
            fail("Exception should be thrown")
        } catch (e: Exception) {
            assertEquals(expect, e.message)
        }
        stopKoin()
    }
}
package example.pokemon.evolve.shared.app.onboarding

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import kotlin.test.Test
import kotlin.test.assertTrue

@OptIn(ExperimentalCoroutinesApi::class)
class OnBoardingViewModelTest {
    private lateinit var vm: OnBoardingViewModel

    /**Should set onnext to true*/
    @Test
    fun nextShouldSetOnNextToTrue() = runTest {
        Dispatchers.setMain(StandardTestDispatcher(testScheduler))
        vm = OnBoardingViewModel()
        vm.next()
        assertTrue(vm.onNext.value)
    }

}
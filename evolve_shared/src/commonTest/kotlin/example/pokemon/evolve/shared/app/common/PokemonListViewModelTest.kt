package example.pokemon.evolve.shared.app.common

import example.pokemon.evolve.shared.domain.evolve.EvolveRepository
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import example.pokemon.evolve.shared.domain.evolve.usecase.*
import kotlinx.coroutines.*
import kotlinx.coroutines.test.*
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import org.koin.test.KoinTest
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class PokemonListViewModelTest : KoinTest {
    private lateinit var vm: PokemonListViewModel

    private fun setup(evolveRepository: EvolveRepository, testScheduler: TestCoroutineScheduler) {
        stopKoin()
        startKoin {
            modules(module {
                singleOf(::GetPokemonsNetworkUseCase)
                singleOf(::GetPokemonsLocalUseCase)
                singleOf(::SetPokemonsLocalUseCase)

                single { evolveRepository }
            })
        }
        Dispatchers.setMain(StandardTestDispatcher(testScheduler))
        vm = PokemonListViewModel()
        vm.useAsyncNetworkCall = false
    }

    /**Should return the pokemons from local when the pokemons are not empty*/
    @Test
    fun getPokemonsLocalWhenPokemonsAreNotEmptyThenReturnThePokemonsFromLocal() = runTest {
        val expect = mutableListOf(PokemonUrl())
        var getPokemonsLocalCount = 0
        val evolveRepository = object : EvolveRepository {
            override suspend fun getPokemonsLocal(
                offset: Int,
                limit: Int,
                search: String,
                isChosen: Boolean?
            ): List<PokemonUrl> {
                getPokemonsLocalCount++
                return expect
            }
        }
        setup(evolveRepository, testScheduler)

        vm.getPokemonsLocal()
        advanceUntilIdle()
        assertEquals(1, getPokemonsLocalCount)
        assertEquals(expect, vm.pokemons.value)
        stopKoin()
    }

    /**Should return the pokemons from network when the pokemons are empty*/
    @Test
    fun getPokemonsLocalWhenPokemonsAreEmptyThenReturnThePokemonsFromNetwork() = runTest {
        var getPokemonsLocalCount = 0
        var getPokemonsNetworkCount = 0
        val evolveRepository = object : EvolveRepository {
            override suspend fun getPokemonsLocal(
                offset: Int,
                limit: Int,
                search: String,
                isChosen: Boolean?
            ): List<PokemonUrl> {
                getPokemonsLocalCount++
                return listOf()
            }

            override suspend fun getPokemonsNetwork(
                offset: Int,
                limit: Int
            ): MutableList<PokemonUrl> {
                getPokemonsNetworkCount++
                return mutableListOf()
            }

            override suspend fun setPokemonsLocal(pokemons: MutableList<PokemonUrl>) = true
        }
        setup(evolveRepository, testScheduler)

        vm.getPokemonsLocal(100000)
        advanceUntilIdle()
        assertEquals(1, getPokemonsLocalCount)
        assertEquals(1, getPokemonsNetworkCount)
        stopKoin()
    }

}
package example.pokemon.evolve.shared.domain.evolve.usecase

import example.pokemon.evolve.shared.domain.evolve.EvolveRepository
import example.pokemon.evolve.shared.domain.evolve.entity.Pokemon
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

@OptIn(ExperimentalCoroutinesApi::class)
class GetPokemonDetailLocalUseCaseTest : KoinTest {
    private lateinit var uc: GetPokemonDetailLocalUseCase
    private fun startKoin(evolveRepository: EvolveRepository) {
        stopKoin()
        org.koin.core.context.startKoin {
            modules(module {
                single { evolveRepository }
            })
        }
    }

    /**Should return the pokemon detail when the pokemon is in local database*/
    @Test
    fun invokeWhenPokemonIsInLocalDatabase() = runTest {
        val expect = "pikachu"
        var getPokemonDetailLocalCount = 0
        val evolveRepository = object : EvolveRepository {
            override suspend fun getPokemonDetailLocal(name: String): Pokemon {
                getPokemonDetailLocalCount++
                return Pokemon().also { it.name = name }
            }
        }
        startKoin(evolveRepository)

        uc = GetPokemonDetailLocalUseCase()
        val actual = uc.invoke(expect)
        assertEquals(1, getPokemonDetailLocalCount)
        assertEquals(expect, actual?.name)
        stopKoin()
    }

    /**Should return null when the pokemon is not in local database*/
    @Test
    fun invokeWhenPokemonIsNotInLocalDatabase() = runTest {
        val expect = "name"
        var getPokemonDetailLocalCount = 0
        val evolveRepository = object : EvolveRepository {
            override suspend fun getPokemonDetailLocal(name: String): Pokemon? {
                getPokemonDetailLocalCount++
                return null
            }
        }
        startKoin(evolveRepository)

        uc = GetPokemonDetailLocalUseCase()
        val actual = uc.invoke(expect)
        assertEquals(1, getPokemonDetailLocalCount)
        assertNull(actual)
        stopKoin()
    }
}
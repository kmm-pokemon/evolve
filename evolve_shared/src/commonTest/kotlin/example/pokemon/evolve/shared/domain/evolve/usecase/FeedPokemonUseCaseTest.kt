package example.pokemon.evolve.shared.domain.evolve.usecase

import example.pokemon.evolve.shared.domain.evolve.entity.Berry
import example.pokemon.evolve.shared.domain.evolve.entity.Pokemon
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class FeedPokemonUseCaseTest {
    private val feedPokemonUseCase = FeedPokemonUseCase()
    private val pokemon = Pokemon()
    private val nextPokemon = Pokemon()
    private val lastEatenBerry = Berry()
    private val givenBerry = Berry()

    /**Should throw an exception when the pokemon weight data is not found*/
    @Test
    fun invokeWhenPokemonWeightDataIsNotFoundThenThrowException() {
        pokemon.weight = null

        try {
            feedPokemonUseCase.invoke(pokemon, nextPokemon, lastEatenBerry, givenBerry)
            fail("Exception should be thrown")
        } catch (e: Exception) {
            assertEquals("Pokemon weight data not found.", e.message)
        }
    }

    /**Should throw an exception when the next pokemon weight data is not found*/
    @Test
    fun invokeWhenNextPokemonWeightDataIsNotFoundThenThrowException() {
        pokemon.weight = 10
        nextPokemon.weight = null

        try {
            feedPokemonUseCase.invoke(pokemon, nextPokemon, lastEatenBerry, givenBerry)
            fail()
        } catch (e: Exception) {
            assertEquals("Next pokemon weight data not found.", e.message)
        }
    }

    /**Should throw an exception when the given berry weight data is not found*/
    @Test
    fun invokeWhenGivenBerryWeightDataIsNotFoundThenThrowException() {
        pokemon.weight = 100
        nextPokemon.weight = 100
        lastEatenBerry.weight = 10
        lastEatenBerry.firmnessName = "soft"
        givenBerry.weight = null

        try {
            feedPokemonUseCase.invoke(pokemon, nextPokemon, lastEatenBerry, givenBerry)
            fail("Should throw exception")
        } catch (e: Exception) {
            assertEquals("Given berry weight data not found.", e.message)
        }
    }

    /**Should return reduced weight twice from given berry when given berry firmness same with last eaten berry firmness*/
    @Test
    fun invokeWhenGivenBerryFirmnessSameWithLastEatenBerryFirmnessThenReturnReducedWeightTwiceGivenBerry() {
        pokemon.weight = 100
        nextPokemon.weight = 100
        lastEatenBerry.weight = 5
        lastEatenBerry.firmnessName = "soft"
        givenBerry.weight = 5
        givenBerry.firmnessName = "soft"

        try {
            val newWeight =
                feedPokemonUseCase.invoke(pokemon, nextPokemon, lastEatenBerry, givenBerry)
            assertEquals(90, newWeight)
        } catch (e: Exception) {
            fail("Should return reduced new weight")
        }
    }

    /**Should throw an exception when the new weight less than zero*/
    @Test
    fun invokeWhenNewWeightLessThanZeroThenThrowException() {
        pokemon.weight = 10
        nextPokemon.weight = 100
        lastEatenBerry.weight = 5
        lastEatenBerry.firmnessName = "soft"
        givenBerry.weight = 5
        givenBerry.firmnessName = "soft"

        try {
            feedPokemonUseCase.invoke(pokemon, nextPokemon, lastEatenBerry, givenBerry)
            fail("Should throw exception")
        } catch (e: Exception) {
            assertEquals("Please, don't give me soft berry again!", e.message)
        }
    }

    /**Should return increased weight twice from given berry when given berry firmness different with last eaten berry firmness*/
    @Test
    fun invokeWhenGivenBerryFirmnessDifferentWithLastEatenBerryFirmnessThenReturnIncreasedWeightGivenBerry() {
        pokemon.weight = 100
        nextPokemon.weight = 100
        lastEatenBerry.weight = 5
        lastEatenBerry.firmnessName = "soft"
        givenBerry.weight = 10
        givenBerry.firmnessName = "hard"

        try {
            val newWeight =
                feedPokemonUseCase.invoke(pokemon, nextPokemon, lastEatenBerry, givenBerry)
            assertEquals(110, newWeight)
        } catch (e: Exception) {
            fail("Should return increased new weight")
        }
    }

}
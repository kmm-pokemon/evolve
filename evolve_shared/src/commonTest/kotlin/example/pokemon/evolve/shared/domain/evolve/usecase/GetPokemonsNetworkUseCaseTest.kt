package example.pokemon.evolve.shared.domain.evolve.usecase

import example.pokemon.evolve.shared.domain.evolve.EvolveRepository
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

@OptIn(ExperimentalCoroutinesApi::class)
class GetPokemonsNetworkUseCaseTest : KoinTest {
    private lateinit var uc: GetPokemonsNetworkUseCase
    private fun startKoin(evolveRepository: EvolveRepository) {
        stopKoin()
        startKoin {
            modules(module {
                single { evolveRepository }
            })
        }
    }

    /**Should return a list of pokemons when the network call is successful*/
    @Test
    fun invokeWhenNetworkCallIsSuccessfulThenReturnListOfPokemons() = runTest {
        val expect = mutableListOf(PokemonUrl())
        var getPokemonsNetworkCount = 0
        val evolveRepository = object : EvolveRepository {
            override suspend fun getPokemonsNetwork(
                offset: Int,
                limit: Int
            ): MutableList<PokemonUrl> {
                getPokemonsNetworkCount++
                return expect
            }
        }
        startKoin(evolveRepository)

        uc = GetPokemonsNetworkUseCase()
        val actual = uc.invoke(0, 10)
        assertEquals(1, getPokemonsNetworkCount)
        assertEquals(expect, actual)
        stopKoin()
    }

    /**Should throw an exception when the network call is not successful*/
    @Test
    fun invokeWhenNetworkCallIsNotSuccessfulThenThrowException() = runTest {
        val expect = "Network call is failed"
        val evolveRepository = object : EvolveRepository {
            override suspend fun getPokemonsNetwork(
                offset: Int,
                limit: Int
            ): MutableList<PokemonUrl> {
                throw Exception(expect)
            }
        }
        startKoin(evolveRepository)

        uc = GetPokemonsNetworkUseCase()
        try {
            uc.invoke(0, 10)
            fail("Exception should be thrown")
        } catch (e: Exception) {
            assertEquals(expect, e.message)
        }
        stopKoin()
    }
}
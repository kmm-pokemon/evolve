package example.pokemon.evolve.shared.domain.evolve.usecase

import example.pokemon.evolve.shared.domain.evolve.EvolveRepository
import example.pokemon.evolve.shared.domain.evolve.entity.Berry
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class GetBerriesLocalUseCaseTest : KoinTest {
    private lateinit var uc: GetBerriesLocalUseCase
    private fun startKoin(evolveRepository: EvolveRepository) {
        stopKoin()
        startKoin {
            modules(module {
                single { evolveRepository }
            })
        }
    }

    /**Should return the list of berries when the list is not empty*/
    @Test
    fun invokeWhenListIsNotEmpty() = runTest {
        val expect = listOf(Berry())
        var getBerriesLocalCount = 0
        val evolveRepository = object : EvolveRepository {
            override suspend fun getBerriesLocal(): List<Berry> {
                getBerriesLocalCount++
                return expect
            }
        }
        startKoin(evolveRepository)

        uc = GetBerriesLocalUseCase()
        val actual = uc.invoke()
        advanceUntilIdle()
        assertEquals(1, getBerriesLocalCount)
        assertEquals(expect, actual)
        stopKoin()
    }

    /**Should return an empty list when the list is empty*/
    @Test
    fun invokeWhenListIsEmpty() = runTest {
        val expect = listOf<Berry>()
        var getBerriesLocalCount = 0
        val evolveRepository = object : EvolveRepository {
            override suspend fun getBerriesLocal(): List<Berry> {
                getBerriesLocalCount++
                return expect
            }
        }
        startKoin(evolveRepository)

        uc = GetBerriesLocalUseCase()
        val actual = uc.invoke()
        advanceUntilIdle()
        assertEquals(1, getBerriesLocalCount)
        assertEquals(expect, actual)
        stopKoin()
    }
}
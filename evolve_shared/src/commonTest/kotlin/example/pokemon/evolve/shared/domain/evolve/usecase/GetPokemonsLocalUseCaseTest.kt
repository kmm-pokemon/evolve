package example.pokemon.evolve.shared.domain.evolve.usecase

import example.pokemon.evolve.shared.domain.evolve.EvolveRepository
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class GetPokemonsLocalUseCaseTest : KoinTest {
    private lateinit var uc: GetPokemonsLocalUseCase
    private fun startKoin(evolveRepository: EvolveRepository) {
        stopKoin()
        startKoin {
            modules(module {
                single { evolveRepository }
            })
        }
    }

    /**Should return the list of pokemons when the list is not empty*/
    @Test
    fun invokeWhenListIsNotEmpty() = runTest {
        val expect = listOf(PokemonUrl())
        var getPokemonsLocalCount = 0
        val evolveRepository = object : EvolveRepository {
            override suspend fun getPokemonsLocal(
                offset: Int,
                limit: Int,
                search: String,
                isChosen: Boolean?
            ): List<PokemonUrl> {
                getPokemonsLocalCount++
                return expect
            }
        }
        startKoin(evolveRepository)

        uc = GetPokemonsLocalUseCase()
        val actual = uc.invoke(0, 1, "", null)
        advanceUntilIdle()
        assertEquals(1, getPokemonsLocalCount)
        assertEquals(expect, actual)
        stopKoin()
    }

    /**Should return an empty list when the list is empty*/
    @Test
    fun invokeWhenListIsEmpty() = runTest {
        val expect = listOf<PokemonUrl>()
        var getPokemonsLocalCount = 0
        val evolveRepository = object : EvolveRepository {
            override suspend fun getPokemonsLocal(
                offset: Int,
                limit: Int,
                search: String,
                isChosen: Boolean?
            ): List<PokemonUrl> {
                getPokemonsLocalCount++
                return expect
            }
        }
        startKoin(evolveRepository)

        uc = GetPokemonsLocalUseCase()
        val actual = uc.invoke(0, 1, "", null)
        advanceUntilIdle()
        assertEquals(1, getPokemonsLocalCount)
        assertEquals(expect, actual)
        stopKoin()
    }
}
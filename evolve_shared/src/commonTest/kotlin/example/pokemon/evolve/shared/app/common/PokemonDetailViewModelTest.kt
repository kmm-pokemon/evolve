package example.pokemon.evolve.shared.app.common

import example.pokemon.evolve.shared.domain.evolve.EvolveRepository
import example.pokemon.evolve.shared.domain.evolve.entity.Berry
import example.pokemon.evolve.shared.domain.evolve.entity.Pokemon
import example.pokemon.evolve.shared.domain.evolve.usecase.*
import kotlinx.coroutines.*
import kotlinx.coroutines.test.*
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import org.koin.test.KoinTest
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class PokemonDetailViewModelTest : KoinTest {
    private lateinit var vm: PokemonDetailViewModel

    private fun setup(
        evolveRepository: EvolveRepository,
        testScheduler: TestCoroutineScheduler
    ) {
        stopKoin()
        startKoin {
            modules(module {
                singleOf(::GetPokemonsLocalUseCase)

                singleOf(::GetPokemonDetailNetworkUseCase)
                singleOf(::GetPokemonDetailLocalUseCase)
                singleOf(::SetPokemonDetailLocalUseCase)
                singleOf(::SetPokemonLocalUseCase)

                singleOf(::GetBerriesNetworkUseCase)
                singleOf(::GetBerriesLocalUseCase)
                singleOf(::FeedPokemonUseCase)

                single { evolveRepository }
            })
        }
        Dispatchers.setMain(StandardTestDispatcher(testScheduler))
        vm = PokemonDetailViewModel()
        vm.useAsyncNetworkCall = false
    }

    /**Should call getpokemondetailnetwork when the pokemon is not in local*/
    @Test
    fun getPokemonDetailLocalWhenPokemonIsNotInLocalThenCallGetPokemonDetailNetwork() = runTest {
        val expect = Pokemon().apply { name = "bulbasaur" }
        var getPokemonDetailNetworkCount = 0
        val evolveRepository = object : EvolveRepository {
            override suspend fun getPokemonDetailNetwork(name: String): Pokemon {
                getPokemonDetailNetworkCount++
                return expect
            }
        }
        setup(evolveRepository, testScheduler)

        vm.getPokemonDetailLocal()
        advanceUntilIdle()
        assertEquals(1, getPokemonDetailNetworkCount)
        assertEquals(expect, vm.pokemon.value)
        stopKoin()
    }

    /**Should return the pokemon when the pokemon is in local*/
    @Test
    fun getPokemonDetailLocalWhenPokemonIsInLocalThenReturnThePokemon() = runTest {
        val expect = Pokemon().apply { name = "bulbasaur" }
        var getPokemonDetailLocalCount = 0
        val evolveRepository = object : EvolveRepository {
            override suspend fun getPokemonDetailLocal(name: String): Pokemon {
                getPokemonDetailLocalCount++
                return expect
            }
        }
        setup(evolveRepository, testScheduler)

        vm.getPokemonDetailLocal()
        advanceUntilIdle()
        assertEquals(1, getPokemonDetailLocalCount)
        assertEquals(expect, vm.pokemon.value)
        stopKoin()
    }

    /**Should return pokemon when the network call is successful*/
    @Test
    fun getPokemonDetailNetworkWhenNetworkCallIsSuccessfulThenReturnPokemon() = runTest {
        val expect = Pokemon().apply { name = "bulbasaur" }
        var getPokemonDetailNetworkCount = 0
        val evolveRepository = object : EvolveRepository {
            override suspend fun getPokemonDetailNetwork(name: String): Pokemon {
                getPokemonDetailNetworkCount++
                return expect
            }
        }
        setup(evolveRepository, testScheduler)

        vm.getPokemonDetailNetwork()
        advanceUntilIdle()
        assertEquals(1, getPokemonDetailNetworkCount)
        assertEquals(expect, vm.pokemon.value)
        stopKoin()
    }

    /**Should update decreased weight when the last eaten berry is same as the current berry*/
    @Test
    fun feedWhenLastEatenBerryIsSameAsCurrentBerryThenUpdateWeight() = runTest {
        val expect = Pokemon().apply { weight = 4 }
        val evolveRepository = object : EvolveRepository {
            override suspend fun setPokemonDetailLocal(pokemon: Pokemon) = true
        }
        setup(evolveRepository, testScheduler)

        vm.lastEatenBerry = Berry().apply { firmnessName = "soft" }
        vm.pokemon.value = Pokemon().apply { weight = 10 }
        vm.nextPokemon.value = Pokemon().apply { weight = 20 }

        vm.feed(Berry().apply { firmnessName = "soft"; weight = 3 })
        advanceUntilIdle()
        assertEquals(expect.weight, vm.pokemon.value?.weight)
        stopKoin()
    }

    /**Should update increased weight when the last eaten berry is different from the given berry*/
    @Test
    fun feedWhenLastEatenBerryIsDifferentFromGivenBerryThenUpdateWeight() = runTest {
        val expect = Pokemon().apply { weight = 110 }
        val evolveRepository = object : EvolveRepository {
            override suspend fun setPokemonDetailLocal(pokemon: Pokemon) = true
        }
        setup(evolveRepository, testScheduler)

        vm.pokemon.value = Pokemon().apply { weight = 100 }
        vm.nextPokemon.value = Pokemon().apply { weight = 200 }

        vm.feed(Berry().apply {
            weight = 10
            firmnessName = "soft"
        })
        advanceUntilIdle()
        assertEquals(expect.weight, vm.pokemon.value?.weight)
        stopKoin()
    }

}